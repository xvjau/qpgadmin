#include "extensionitem.h"

ExtensionItem::ExtensionItem(TreeItem *_parent, const QSqlRecord &_record):
    TreeItem(_parent, _record, _S("extension"))
{
    setNoChildren();
}
