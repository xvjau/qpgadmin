#pragma once

#include "genericsection.h"
#include "foreigntableitem.h"

class ForeignTablesSection : public GenericSection
{
public:
    ForeignTablesSection(TreeItem *_parent);

    ForeignTableItem *addItem(const QSqlRecord &_record)
    {
        return make_child<ForeignTableItem>(_record);
    }
};
