#include "treeitem.h"

#include <QSqlError>

#include <cassert>
#include <unordered_map>

TreeItem::TreeItem(QObject *_parent):
    QObject(_parent)
{
}

TreeItem::TreeItem(TreeItem *_parent, const QString &title, const QString &icon, const QString &_description):
    QObject(_parent),
    m_title(title),
    m_description(_description)
{
    if (_parent)
    {
        connect(this, &TreeItem::beginLoad, _parent, &TreeItem::beginLoad);
        connect(this, &TreeItem::endLoad, _parent, &TreeItem::endLoad);
        connect(this, &TreeItem::beginUnload, _parent, &TreeItem::beginUnload);
        connect(this, &TreeItem::endUnload, _parent, &TreeItem::endUnload);
    }
    changeIcon(icon);
}

TreeItem::TreeItem(TreeItem *_parent, const QSqlRecord &_record, const QString &icon, const QString &_description):
    TreeItem(_parent, _record.value(0).toString(), icon, _description)
{
}

void TreeItem::setNoChildren()
{
    m_state = stLoaded;
    auto _parent = qobject_cast<TreeItem*>(parent());
    if (_parent)
    {
        disconnect(this, &TreeItem::beginLoad, _parent, &TreeItem::beginLoad);
        disconnect(this, &TreeItem::endLoad, _parent, &TreeItem::endLoad);
        disconnect(this, &TreeItem::beginUnload, _parent, &TreeItem::beginUnload);
        disconnect(this, &TreeItem::endUnload, _parent, &TreeItem::endUnload);
    }
}

void TreeItem::sort()
{
    std::sort(m_children.begin(), m_children.end(), [](const TreeItem* a, const TreeItem* b){ return a->title().compare(b->title(), Qt::CaseInsensitive) < 0; });
}

void TreeItem::beginLoadChildren(int _count)
{
    m_state = stLoading;
    emit beginLoad(this, _count);
    if (m_children.size() == 1 && m_children[0] == nullptr)
        m_children.clear();
}

void TreeItem::load()
{
    if (state() == stUnloaded)
    {
        if (m_children.size())
        {
            beginLoadChildren(m_children.size());
            endLoadChildren();
        }
        else
        {
            m_state = stLoaded;
        }
    }
}

void TreeItem::endLoadChildren()
{
    m_state = stLoaded;
    emit endLoad();
}

void TreeItem::beginUnloadChildren(int _count)
{
    emit beginUnload(this, _count);
    m_state = stUnloading;
}
void TreeItem::unload()
{
    for(auto p : m_children)
        p->deleteLater();
    m_children.clear();
}

void TreeItem::unload(TreeItem *_child)
{
    auto count = childCount();

    auto it = std::find(std::begin(m_children), std::end(m_children), _child);
    if (it != std::end(m_children))
    {
        emit beginUnload(*it, 1);
        m_children.erase(it);
        _child->deleteLater();
        emit endUnload();
    }
    else
    {
        DEBUG_MSG(_child->title() << " not found!" << endl);
        assert(it != std::end(m_children));
    }
#ifndef NDEBUG
    assert(childCount() == count - 1);
#endif
}

void TreeItem::endUnloadChildren()
{
    m_state = stUnloaded;
    emit endUnload();
}

int TreeItem::row() const
{
    auto p = qobject_cast<TreeItem*>(parent());
    if (p)
    {
        auto r = 0;
        for(auto i : p->m_children)
            if (i == this)
                return r;
            else
                r++;

        assert(false);
    }
    return -1;
}

namespace
{

std::unordered_map<QString, QPixmap> s_pixmapcache;

}

void TreeItem::changeIcon(const QString &_icon)
{
    auto it = s_pixmapcache.find(_icon);
    if (it != s_pixmapcache.end())
    {
        m_icon = it->second;
    }
    else
    {
        auto ret = m_icon.load(_S(":/schema/%1.png").arg(_icon));

        if (ret)
        {
            s_pixmapcache[_icon] = m_icon;
        }
#ifndef NDEBUG
        else
        {
            DEBUG_MSG("Resource not found: '" << _icon << "'\n\n");

            for(const auto& i:  QDir(_S(":/schema/")).entryList())
            {
                DEBUG_MSG(i << "\n");
            }

            DEBUG_MSG(flush);
        }
#endif
        assert(ret);
        assert(!m_icon.isNull());
    }
}

QVariant TreeItem::data(int column, int role, bool noCount) const
{
    if (column == 0)
    {
        switch(role)
        {
            case Qt::DisplayRole:
            {
                auto result = description().isEmpty() ? title() : title() + _S(" (") + description() + _S(")");
                if (!noCount)
                    result += _S(" (%1)").arg(m_children.size());
                return result;
            }
            case Qt::DecorationRole: return icon();
        }
    }
    return QVariant();
}

std::tuple<bool,QString> TreeItem::loadSQL(const QString &_section, const QString &_name) const
{
    auto ver = _S("default");
    return readResource(_S(":/sqls/%1/%2/%3.sql").arg(_section, ver, _name));
}

std::tuple<bool,QSqlQuery> TreeItem::loadQuery(QSqlDatabase& db, const QString& _section, const QString& _name, const std::initializer_list<ParamBindings>& bindings) const
{
    auto [success, sql] = loadSQL(_section, _name);
    if (!success || sql.isEmpty())
    {
        DEBUG_MSG("WARNING - Sql not found: " << _section << ":" << _name << endl);
        return {false, QSqlQuery()};
    }

    for(const auto& bind : bindings)
    {
        switch((QMetaType::Type)bind.value.type())
        {
            case QMetaType::Void:
            case QMetaType::Nullptr:
                sql.replace(bind.name, _S(""));
                break;

            case QMetaType::Bool:
                sql.replace(bind.name, (bind.value.toBool() ? _S("TRUE") : _S("FALSE")));
                break;

            case QMetaType::Int:
            case QMetaType::UInt:
            case QMetaType::Double:
            case QMetaType::Long:
            case QMetaType::LongLong:
            case QMetaType::Short:
            case QMetaType::Char:
            case QMetaType::ULong:
            case QMetaType::ULongLong:
            case QMetaType::UShort:
            case QMetaType::SChar:
            case QMetaType::UChar:
            case QMetaType::Float:
                sql.replace(bind.name, bind.value.toString());
                break;

            case QMetaType::VoidStar:
            case QMetaType::QChar:
            case QMetaType::QString:
            case QMetaType::QByteArray:
            case QMetaType::QObjectStar:
            case QMetaType::QVariant:
            case QMetaType::QCursor:
            case QMetaType::QDate:
            case QMetaType::QSize:
            case QMetaType::QTime:
            case QMetaType::QVariantList:
            case QMetaType::QPolygon:
            case QMetaType::QPolygonF:
            case QMetaType::QColor:
            case QMetaType::QSizeF:
            case QMetaType::QRectF:
            case QMetaType::QLine:
            case QMetaType::QTextLength:
            case QMetaType::QStringList:
            case QMetaType::QVariantMap:
            case QMetaType::QVariantHash:
            case QMetaType::QIcon:
            case QMetaType::QPen:
            case QMetaType::QLineF:
            case QMetaType::QTextFormat:
            case QMetaType::QRect:
            case QMetaType::QPoint:
            case QMetaType::QUrl:
            case QMetaType::QRegExp:
            case QMetaType::QRegularExpression:
            case QMetaType::QDateTime:
            case QMetaType::QPointF:
            case QMetaType::QPalette:
            case QMetaType::QFont:
            case QMetaType::QBrush:
            case QMetaType::QRegion:
            case QMetaType::QBitArray:
            case QMetaType::QImage:
            case QMetaType::QKeySequence:
            case QMetaType::QSizePolicy:
            case QMetaType::QPixmap:
            case QMetaType::QLocale:
            case QMetaType::QBitmap:
            case QMetaType::QMatrix:
            case QMetaType::QTransform:
            case QMetaType::QMatrix4x4:
            case QMetaType::QVector2D:
            case QMetaType::QVector3D:
            case QMetaType::QVector4D:
            case QMetaType::QQuaternion:
            case QMetaType::QEasingCurve:
            case QMetaType::QJsonValue:
            case QMetaType::QJsonObject:
            case QMetaType::QJsonArray:
            case QMetaType::QJsonDocument:
            case QMetaType::QModelIndex:
            case QMetaType::QPersistentModelIndex:
            case QMetaType::QUuid:
            case QMetaType::QByteArrayList:
            case QMetaType::User:
            case QMetaType::UnknownType:
            default:
            {
                auto s = bind.value.toString();
                s.replace(_C('\\'), _S("\\\\"));
                s.replace(_C('\''), _S("\\'"));
                sql.replace(bind.name, _S("'%1'").arg(s));
                break;
            }
        }
    }

    CursorGuard cg;

    auto qry = QSqlQuery(db);
    auto result = qry.exec(sql);

    if (!result)
    {
        cg.reset();
        auto error = qry.lastError();
        DEBUG_MSG(tr("Error executing query %1:%2\n").arg(_section, _name)
                    << error.driverText() << "\n" << error.databaseText() << endl);
    }
    assert(qry.isActive());
    return {true, qry};
}
