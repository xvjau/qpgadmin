#include "treeitem.h"
#include "genericsection.h"
#include "foreigntablessection.h"

ForeignTablesSection::ForeignTablesSection(TreeItem *_parent):
    GenericSection(_parent, tr("Foreign Tables"), _S("foreigntables"))
{
}
