#include "foreigndatawrapperitem.h"

ForeignDataWrapperItem::ForeignDataWrapperItem(TreeItem *_parent, const QSqlRecord &_record):
    RelationItem(_parent, _record, _S("foreigndatawrapper"))
{
}

void ForeignDataWrapperItem::getSQL()
{
    if (m_sql.isEmpty())
    {
        m_sql = _S("--This is still incomplete.\n\ncreate foreign data wrapper %1;").arg(title());

        //[ HANDLER handler_function | NO HANDLER ]
        //[ VALIDATOR validator_function | NO VALIDATOR ]
        //[ OPTIONS ( option 'value' [, ... ] ) ]
    }

    emit sql(m_sql);
}
