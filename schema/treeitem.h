#pragma once

#include "main.h"

#include <new>
#include <cassert>
#include <vector>
#include <memory>
#include <QSqlRecord>
#include <QString>
#include <QVariant>
#include <QPixmap>
#include <QSqlDatabase>
#include <QSqlQuery>

#include <QDir>

class TreeItem : public QObject
{
    Q_OBJECT
public:
    enum ItemType
    {
        itNULL,
        itCatalog,
        itDatabase,
        itExtension,
        itForeignDataWrapper,
        itForeignTable,
        itFunction,
        itGenericSection,
        itGroupRole,
        itIndex,
        itLanguage,
        itLoginRole,
        itNamespace,
        itSchema,
        itSequence,
        itServer,
        itServerGroup,
        itServerGroups,
        itTable,
        itTableSpace,
        itTrigger,
        itTriggerFunction,
        itType,
        itView,
        itMaterializedView,
    };

    enum State
    {
        stLoaded,
        stUnloaded,
        stLoading,
        stUnloading,
    };

private:
    QString m_title;
    ItemType m_type = itNULL;
    QPixmap m_icon;
    QString m_description;

    State m_state = stUnloaded;
    std::vector<TreeItem *> m_children;

public:
    virtual void load();

protected:
    virtual void unload();
    virtual void unload(TreeItem *_child);

    virtual void beginLoadChildren(int _count);
    virtual void endLoadChildren();
    virtual void beginUnloadChildren(int _count);
    virtual void endUnloadChildren();

    template<typename T, typename... _Args>
    T *make_child(_Args &&... __args)
    {
        // First, allocate the memory for the object
        auto p = ::operator new(sizeof(T));
        // Make sure that the pointer to the new object is present in the
        // m_children _before_ the ctor is called.  This makes sure that
        // if the child inquires it's parent about itself, the parent has
        // the child in the list
        m_children.push_back(static_cast<T *>(p));
        // Now call the ctor 'normally' with the pointer alllocated
        return new(p) T(this, std::forward<_Args>(__args)...);
    }

    [[nodiscard]] State state() const
    {
        return m_state;
    }
    void setTitle(const QString &_title)
    {
        m_title = _title;
    }
    void setDescription(const QString &_description)
    {
        m_description = _description;
    }

signals:
    void beginLoad(TreeItem *, int count);
    void endLoad();

    void beginUnload(TreeItem *, int count);
    void endUnload();

public:
    TreeItem(QObject *_parent);
    TreeItem(TreeItem *_parent, const QString &_title, const QString &_icon, const QString &_description = QString());
    TreeItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon, const QString &_description = QString());
    virtual ~TreeItem() {}

    [[nodiscard]] QString title() const
    {
        return m_title;
    }
    [[nodiscard]] QPixmap icon() const
    {
        return m_icon;
    }
    [[nodiscard]] QString description() const
    {
        return m_description;
    }

    [[nodiscard]] TreeItem *child(int index)
    {
        if(m_state == stUnloaded)
        {
            load();

            if(index >= (int)m_children.size())
            {
                // This might happen if after a load, we become empty
                // Just send a fake result to avoid AV, after the signal is
                // handled, the model should remove the row
                return nullptr;
            }
        }

        if(m_state == stLoaded)
            return m_children[index];
        else
            return nullptr;
    }

    TreeItem *parentItem()
    {
        return qobject_cast<TreeItem *>(parent());
    }
    int row() const;

    int childCount() const
    {
        return m_children.size();
    }
    virtual int columnCount() const
    {
        return 1;
    }

    virtual QVariant data(int column, int role, bool noCount = false) const;

    void setNoChildren();

    template<typename T>
    void make_children(const QString &_title, const QString &_icon, QSqlDatabase &_db, const QString &_select);

    template<typename T>
    void make_children(const QString &_title, const QString &_icon, QSqlQuery _query);

    template<typename T>
    void make_children(const QString &_title, const QString &_icon, std::tuple<bool, QSqlQuery> _query);

    template<typename T, typename U>
    void make_children(const QString &_title, const QString &_icon, const U &_container);

    template<typename T>
    [[nodiscard]] T *findParent()
    {
        T *result = nullptr;
        auto p = this;

        while(result == nullptr && p)
        {
            result = dynamic_cast<T *>(p);
            p = p->parentItem();
        }

        return result;
    }

    void sort();

    [[nodiscard]] std::tuple<bool, QString> loadSQL(const QString &_section, const QString &_name) const;

    struct ParamBindings
    {
        QString name;
        QVariant value;
    };
    [[nodiscard]] std::tuple<bool, QSqlQuery> loadQuery(QSqlDatabase &db, const QString &_section, const QString &_name, const std::initializer_list<ParamBindings> &bindings = {}) const;

    void changeIcon(const QString &_icon);

    virtual ItemType type() const = 0;
};

#include "genericsection.h"

template<typename T>
void TreeItem::make_children(const QString &_title, const QString &_icon, QSqlDatabase &_db, const QString &_select)
{
    auto qry = QSqlQuery(_select, _db);
    make_children<T>(_title, _icon, qry);
}

template<typename T>
void TreeItem::make_children(const QString &_title, const QString &_icon, QSqlQuery _query)
{
    auto section = make_child<GenericSection>(_title, _icon);

    if(!_query.isActive())
        _query.exec();

    auto count = _query.numRowsAffected();

    if(count > 0)
    {
        decltype(count) rows = 0;
        section->beginLoadChildren(count);

        while(_query.next())
        {
            section->make_child<T>(_query.record());
            rows++;
        }

        assert(rows == count);
        section->endLoadChildren();
    }
}

template<typename T>
void TreeItem::make_children(const QString &_title, const QString &_icon, std::tuple<bool, QSqlQuery> _query)
{
    auto [ sucess, qry ] = std::move(_query);
    assert(sucess);
    make_children<T>(_title, _icon, std::move(qry));
}

template<typename T, typename U>
void TreeItem::make_children(const QString &_title, const QString &_icon, const U &_container)
{
    auto section = make_child<GenericSection>(_title, _icon);
    auto count = std::size(_container);

    if(count > 0)
    {
        decltype(count) rows = 0;
        section->beginLoadChildren(count);

        for(const auto &i : _container)
        {
            section->make_child<T>(i);
            rows++;
        }

        assert(rows == count);
        section->endLoadChildren();
    }
}
