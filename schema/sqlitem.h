#pragma once

#include "treeitem.h"
#include "statsmodel.h"
#include <memory>
#include <QProcess>

class SQLItem: public TreeItem
{
    Q_OBJECT

public:
    SQLItem(TreeItem *_parent, const QString &_title, const QString &_icon, const QString &_sql = QString());
    SQLItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_title, const QString &_icon, const QString &_sql = QString());
    SQLItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon, const QString &_sql = QString());

    virtual QVariant data(int column, int role, bool) const override;

signals:
    void sql(const QString &sql);

public:
    virtual void getSQL();

    oid_t oid() const { return m_oid; }


    StatsModel* properties()
    {
        if (!m_statsIsLoaded)
            loadStats();

        return m_properties.get();
    }
    StatsModel* stats()
    {
        if (!m_statsIsLoaded)
            loadStats();

        return m_stats.get();
    }
    StatsModel* dependencies()
    {
        if (!m_statsIsLoaded)
            loadStats();

        return m_dependencies.get();
    }
    StatsModel* dependents()
    {
        if (!m_statsIsLoaded)
            loadStats();

        return m_dependents.get();
    }

private:
    bool m_statsIsLoaded = false;
    std::unique_ptr<QProcess> m_process;

protected:
    oid_t m_oid = 0;
    QString m_sql;

    std::unique_ptr<StatsModel> m_properties = nullptr;
    std::unique_ptr<StatsModel> m_stats = nullptr;
    std::unique_ptr<StatsModel> m_dependencies = nullptr;
    std::unique_ptr<StatsModel> m_dependents = nullptr;

    virtual void loadStats();
};


