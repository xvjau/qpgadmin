#pragma once

#include "treeitem.h"

class TablespaceItem : public TreeItem
{
public:
    TablespaceItem(TreeItem *_parent, const QString &_title);
    TablespaceItem(TreeItem *_parent, const QSqlRecord &_record);

    virtual ItemType type() const override { return itTableSpace; };
};
