#pragma once

#include "treeitem.h"

class CatalogItem : public TreeItem
{
public:
    CatalogItem(TreeItem *_parent);

    virtual ItemType type() const override { return itCatalog; };

protected:
    virtual void load() override;
};


