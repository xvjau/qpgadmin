#include "grouprolesitem.h"

GroupRolesItem::GroupRolesItem(TreeItem *_parent, const QSqlRecord &_record):
    TreeItem(_parent, _record, _S("groups"))
{
    setNoChildren();
}

QVariant GroupRolesItem::data(int column, int role, bool) const
{
    return TreeItem::data(column, role, true);
}
