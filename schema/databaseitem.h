#pragma once

#include "treeitem.h"
#include <memory>

class DatabaseItem: public TreeItem
{
    Q_OBJECT

public:
    DatabaseItem(TreeItem *_parent, const QSqlRecord &_record);

    QSqlDatabase db;
    QVariant handle();

    QVariant data(int column, int role, bool) const override;

    virtual ItemType type() const override
    {
        return itDatabase;
    }

    void disconnectDatabase();

signals:
    void connected(DatabaseItem *);
    void error(DatabaseItem *, const QString &_message);
    void disconnected(DatabaseItem *);

protected:
    virtual void load() override;

    void loadForeignDataWrappers();
};
