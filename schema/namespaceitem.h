#pragma once

#include "treeitem.h"

class NamespaceItem : public TreeItem
{
public:
    NamespaceItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon = _S("namespace"));

    virtual QVariant data(int column, int role, bool) const;

    virtual ItemType type() const override { return itNamespace; }
};
