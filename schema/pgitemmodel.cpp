#include "pgitemmodel.h"
#include "main.h"

#include "serveritem.h"
#include "genericsection.h"
#include "servergroups.h"

#include <cassert>
#include <stack>

PGItemModel::PGItemModel():
    QAbstractItemModel()
{
    m_rootItem = std::make_shared<ServerGroups>(nullptr);

    connect(m_rootItem.get(), &TreeItem::beginLoad, this, &PGItemModel::onTreeItemBeginLoad);
    connect(m_rootItem.get(), &TreeItem::endLoad, this, &PGItemModel::onTreeItemEndLoad);
    connect(m_rootItem.get(), &TreeItem::beginUnload, this, &PGItemModel::onTreeItemBeginUnLoad);
    connect(m_rootItem.get(), &TreeItem::endUnload, this, &PGItemModel::onTreeItemEndUnLoad);
}

PGItemModel::PGItemModel(QSqlDatabase &db):
    QAbstractItemModel()
{
    m_rootItem = std::make_shared<ServerItem>(nullptr, db);

    connect(m_rootItem.get(), &TreeItem::beginLoad, this, &PGItemModel::onTreeItemBeginLoad);
    connect(m_rootItem.get(), &TreeItem::endLoad, this, &PGItemModel::onTreeItemEndLoad);
    connect(m_rootItem.get(), &TreeItem::beginUnload, this, &PGItemModel::onTreeItemBeginUnLoad);
    connect(m_rootItem.get(), &TreeItem::endUnload, this, &PGItemModel::onTreeItemEndUnLoad);
}

int PGItemModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return m_rootItem->columnCount();
}

QVariant PGItemModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

    return item->data(index.column(), role);
}

Qt::ItemFlags PGItemModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index);
}

QVariant PGItemModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return m_rootItem->data(section, role);

    return QVariant();
}

QModelIndex PGItemModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = m_rootItem.get();
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex PGItemModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parentItem();

    if (parentItem == m_rootItem.get() || !parentItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int PGItemModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = m_rootItem.get();
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

void PGItemModel::onTreeItemBeginLoad(TreeItem* item, int _count)
{
    const auto row = item->row();
    auto idx = createIndex(row, 0, item);
    beginInsertRows(idx, 0, _count - 1);
}

void PGItemModel::onTreeItemEndLoad()
{
    endInsertRows();
}

void PGItemModel::onTreeItemBeginUnLoad(TreeItem* item, int _count)
{
    auto parentIdx = createIndex(item->row(), 0, item);
    beginRemoveRows(parentIdx, 0, _count - 1);
}

void PGItemModel::onTreeItemEndUnLoad()
{
    endRemoveRows();
}

void PGItemModel::loadItem(const QModelIndex &index)
{
    auto item = static_cast<TreeItem*>(index.internalPointer());
    item->load();
}

QModelIndex PGItemModel::indexOf(TreeItem *_item)
{
    assert(_item != m_rootItem.get());
    return createIndex(_item->row(), 0, _item);
}

void PGItemModel::addGroup(QString name)
{
    auto sg = dynamic_cast<ServerGroups*>(m_rootItem.get());
    auto count = sg->childCount();
    beginInsertRows(QModelIndex(), count, count);
    sg->addGroup(name);
    endInsertRows();
}

#ifndef NDEBUG

QModelIndex PGItemModel::findItem(int _type, const QString &_title, const QModelIndex &_parent)
{
    auto count = rowCount(_parent);
    for(int i = 0; i < count; i++)
    {
        auto idx = index(i, 0, _parent);
        auto item = static_cast<TreeItem*>(idx.internalPointer());
        if (item && item->type() == _type && item->title() == _title)
        {
            return idx;
        }
    }

    for(int i = 0; i < count; i++)
    {
        auto idx = index(i, 0, _parent);
        auto ret = findItem(_type, _title, idx);
        if (ret.isValid())
            return ret;
    }

    return QModelIndex();
}

#endif
