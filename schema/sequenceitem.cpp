#include "sequenceitem.h"

#include "databaseitem.h"
#include "schemaitem.h"

SequenceItem::SequenceItem(TreeItem *_parent, const QSqlRecord &_record):
    SQLItem(_parent, _record, _S("sequence"))
{
}

void SequenceItem::getSQL()
{
    if (m_sql.isEmpty())
    {
        CursorGuard cg;
        auto di = findParent<DatabaseItem>();
        assert(di);
        auto db = di->db;
        auto hnd = di->handle();
        auto si = findParent<SchemaItem>();

        auto schema = escapeIdentifier(hnd, si->title());
        auto seq = escapeIdentifier(hnd, title());

        auto qry = QSqlQuery(db);
        qry.exec(_S("select last_value from %1.%2").arg(schema, seq));
        qry.next();

        auto startWith = qry.value(_S("last_value")).toString();
        auto incrementBy = QString::number(1);
        auto cache = QString::number(1);

/*
        m_sql = _S(R"sql(create sequence %1.%2
start with %3
increment by %4
no minvalue
no maxvalue
cache %5;)sql").arg(schema, seq, startWith, incrementBy, cache);
*/
        m_sql = _S(R"sql(create sequence %1.%2
start with %3;)sql").arg(schema, seq, startWith);
    }

    emit sql(m_sql);
}
