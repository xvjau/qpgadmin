#include "treeitem.h"
#include "genericsection.h"

GenericSection::GenericSection(TreeItem *_parent, const QString &_title, const QString &_icon):
    TreeItem(_parent, _title, _icon)
{
}

GenericSection::GenericSection(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon):
    TreeItem(_parent, _record, _icon)
{
}
