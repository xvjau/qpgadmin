select
    i.*
from
    pg_class c
        join pg_namespace n on n.oid = c.relnamespace
        join    information_schema.columns i  on c.relname = i.table_name and n.nspname = i.table_schema
where
    c.oid = :oid
order by i.ordinal_position
