select c.oid, c.relname, a.rolname as "relowner",coalesce(ts.spcname, 'pg_global') as "reltablespace", c.*,
       pi.tablename as "index_table", pg_get_indexdef(c.oid)
from pg_class c
    left join pg_namespace ns on c.relnamespace = ns.oid
    left join pg_roles a on a.oid = c.relowner
    left join pg_tablespace ts on ts.oid = c.reltablespace
    left join pg_indexes pi on pi.schemaname = nspname and pi.indexname = c.relname
where nspname = :name
order by c.relname
