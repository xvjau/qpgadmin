select t.oid, typname, typtype
from pg_type t
    join pg_namespace ns on t.typnamespace = ns.oid
where nspname = :name and not typtype in ('b', 'c')
order by typname
