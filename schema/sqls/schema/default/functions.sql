select p.oid, proname, t.typname as "prorettype",
	case
		when p.prokind = 'f' then pg_get_functiondef(p.oid)
	end as "pg_get_functiondef",
	pg_get_function_arguments(p.oid)
from pg_proc p
	join pg_namespace ns on p.pronamespace = ns.oid
	join pg_type t on p.prorettype = t.oid
where nspname = :name
order by 2
