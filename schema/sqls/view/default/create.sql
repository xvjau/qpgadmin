select
    c.relname, n.nspname, pg_get_viewdef(c.oid, true)
from
    pg_class c
        join pg_namespace n on n.oid = c.relnamespace
where
    c.oid = :oid
