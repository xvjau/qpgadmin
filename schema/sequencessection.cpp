#include "treeitem.h"
#include "genericsection.h"
#include "sequencessection.h"

SequencesSection::SequencesSection(TreeItem *_parent):
    GenericSection(_parent, tr("Sequences"), _S("tables"))
{
}
