#include "indexitem.h"

IndexItem::IndexItem(TreeItem *_parent, const QString &_name, const QString &_sql):
    SQLItem(_parent, _name, _S("index"), _sql)
{
    setNoChildren();
}

IndexItem::IndexItem(TreeItem *_parent, const std::pair<const QString&, const QString&>& _indexdef):
    IndexItem(_parent, _indexdef.first, _indexdef.second)
{
}

