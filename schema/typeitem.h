#pragma once

#include "sqlitem.h"

class TypeItem : public SQLItem
{
public:
    TypeItem(TreeItem *_parent, const QSqlRecord &_record);

    virtual ItemType type() const override { return itType; };
};
