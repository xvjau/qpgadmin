#include "databaseitem.h"

#include <cassert>

#include <QUuid>
#include <QSqlError>
#include <QSqlDriver>

#include "schemaitem.h"
#include "serveritem.h"
#include "catalogitem.h"
#include "relationitem.h"
#include "extensionitem.h"
#include "languageitem.h"
#include "foreigndatawrapperitem.h"

DatabaseItem::DatabaseItem(TreeItem *_parent, const QSqlRecord &_record) :
    TreeItem(_parent, _record, _S("databasebad"))
{
}

QVariant DatabaseItem::handle()
{
    return db.driver()->handle();
}

void DatabaseItem::load()
{
    if (state() == stUnloaded)
    {
        auto si = findParent<ServerItem>();
        assert(si);

        db = QSqlDatabase::cloneDatabase(si->db, QUuid::createUuid().toString());
        db.setConnectOptions(_S("dbname=%1").arg(title()));

        CursorGuard cg;

        DEBUG_MSG("Database connect: " << db << "\n");
        auto result = db.open();

        if (result)
        {
            const auto count = 5;
            beginLoadChildren(count);

            make_child<CatalogItem>();
            make_children<ExtensionItem>(tr("Extensions"), _S("extensions"), db, _S("select extname from pg_catalog.pg_extension order by extname"));
            make_children<LanguageItem>(tr("Languages"), _S("languages"), db, _S("select lanname from pg_catalog.pg_language where lanispl order by lanname"));
            make_children<SchemaItem>(tr("Schemas"), _S("namespaces"), db, _S("select schema_name from information_schema.schemata where schema_name != 'information_schema' and schema_name !~ 'pg_.*' order by schema_name"));
            make_children<ForeignDataWrapperItem>(tr("Foreign Data Wrappers"), _S("foreigndatawrappers"), loadQuery(db, _S("database"), _S("foreign_data_wrappers")));

            changeIcon(_S("database"));
            sort();

            cg.reset();

            endLoadChildren();
            assert(childCount() == count);
            emit connected(this);
        }
        else
        {
            cg.reset();

            DEBUG_MSG("Opened error: " << title() << " " << db.lastError().databaseText() << endl);
            emit error(this, db.lastError().databaseText());
        }
    }
}

QVariant DatabaseItem::data(int column, int role, bool) const
{
    return TreeItem::data(column, role, true);
}

void DatabaseItem::disconnectDatabase()
{
    beginUnloadChildren(childCount());
    db.close();
    changeIcon(_S("databasebad"));
    unload();
    endUnloadChildren();
}
