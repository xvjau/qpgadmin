#pragma once

#include "sqlitem.h"

#include <vector>

class RelationItem : public SQLItem
{
public:
    RelationItem(TreeItem *_parent, const QString &_title, const QString &_icon = _S("table"));
    RelationItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon = _S("table"));


    struct Field
    {
        Field(QString &&_name, QString &&_type, QString &&_defaultValue, bool _nullable = true);

        QString name;
        QString type;
        QString defaultValue;
        bool nullable = true;
    };
    typedef std::vector<Field> FieldList;

    const FieldList &fieldList() const
    {
        return m_fieldList;
    }

protected:
    FieldList m_fieldList;

    virtual void loadStats() override;
    virtual void getSQL() override;
};
