#pragma once

#include "relationitem.h"

class ViewItem : public RelationItem
{
public:
    ViewItem(TreeItem *_parent, const QSqlRecord &_record, bool _isMeterialized);

    virtual ItemType type() const override { return m_isMeterialized ? itMaterializedView : itView; }

protected:
    bool m_isMeterialized = false;
};
