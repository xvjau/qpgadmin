#pragma once

#include "sqlitem.h"

class TriggerItem : public SQLItem
{
public:
    TriggerItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_title, const QString &_sql = QString());

    virtual ItemType type() const override { return itTrigger; };
};
