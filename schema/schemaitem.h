#pragma once

#include "treeitem.h"

class SchemaItem: public TreeItem
{
public:
    SchemaItem(TreeItem *_parent, const QString &_name, const QString &_title, const QString &_icon);
    SchemaItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon = _S("namespace"));

    virtual QVariant data(int column, int role, bool) const override;

    QString name() const
    {
        return m_name;
    }
    virtual ItemType type() const override
    {
        return itSchema;
    }

    void loadRelationObjects(QSqlDatabase &db);
    void loadFunctions(QSqlDatabase &db);
    void loadTypes(QSqlDatabase &db);

protected:
    QString m_name;
    virtual void load() override;

};
