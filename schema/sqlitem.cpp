#include "sqlitem.h"
#include "schemaitem.h"
#include "databaseitem.h"

#include <cassert>

SQLItem::SQLItem(TreeItem *_parent, const QString &_title, const QString &_icon, const QString &_sql):
    TreeItem(_parent, _title, _icon),
    m_sql(_sql)
{
    setNoChildren();
}

SQLItem::SQLItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon, const QString &_sql):
    SQLItem(_parent, _record, _record.value(1).toString(), _icon, _sql)
{
}

SQLItem::SQLItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_title, const QString &_icon, const QString &_sql):
    TreeItem(_parent, _title, _icon),
    m_sql(_sql)
{
    setNoChildren();
    m_oid = _record.value(0).value<oid_t>();
    assert(m_oid > 0);

    m_properties = std::make_unique<StatsModel>();
    m_properties->addRow(tr("Name"), title());
    m_properties->addRow(tr("OID"), m_oid);

    m_properties->addRow(tr("Namespace"), _record.value(_S("relnamespace")).toString(), tr("The OID of the namespace that contains this relation"));
    m_properties->addRow(tr("Data Type OID"), _record.value(_S("reltype")).toString(), tr("The OID of the data type that corresponds to this table's row type, if any (zero for indexes, which have no pg_type entry)"));
    m_properties->addRow(tr("Underlying Composite type OID"), _record.value(_S("reloftype")).toString(), tr("For typed tables, the OID of the underlying composite type, zero for all other relations"));
    m_properties->addRow(tr("Owner"), _record.value(_S("relowner")).toString(), tr("Owner of the relation"));
    m_properties->addRow(tr("Acess Method"), _record.value(_S("relam")).toString(), tr("If this is an index, the access method used (B-tree, hash, etc.)"));
    m_properties->addRow(tr("On-disk File"), _record.value(_S("relfilenode")).toString(), tr("Name of the on-disk file of this relation; zero means this is a “mapped” relation whose disk file name is determined by low-level state"));
    m_properties->addRow(tr("Tablespace"), _record.value(_S("reltablespace")).toString(), tr("The tablespace in which this relation is stored. If zero, the database's default tablespace is implied. (Not meaningful if the relation has no on-disk file.)"));
    m_properties->addRow(tr("On-disk Size (pages)"), _record.value(_S("relpages")).toString(), tr("Size of the on-disk representation of this table in pages (of size BLCKSZ). This is only an estimate used by the planner. It is updated by VACUUM, ANALYZE, and a few DDL commands such as CREATE INDEX."));
    m_properties->addRow(tr("Rows (estimated)"), _record.value(_S("reltuples")).toString(), tr("Number of rows in the table. This is only an estimate used by the planner. It is updated by VACUUM, ANALYZE, and a few DDL commands such as CREATE INDEX."));
    m_properties->addRow(tr("Pages Marked All-visible"), _record.value(_S("relallvisible")).toString(), tr("Number of pages that are marked all-visible in the table's visibility map. This is only an estimate used by the planner. It is updated by VACUUM, ANALYZE, and a few DDL commands such as CREATE INDEX."));
    m_properties->addRow(tr("TOAST OID"), _record.value(_S("reltoastrelid")).toString(), tr("OID of the TOAST table associated with this table, 0 if none. The TOAST table stores large attributes “out of line” in a secondary table."));
    m_properties->addRow(tr("Has Index?"), _record.value(_S("relhasindex")).toString(), tr("True if this is a table and it has (or recently had) any indexes"));
    m_properties->addRow(tr("Is Shared?"), _record.value(_S("relisshared")).toString(), tr("True if this table is shared across all databases in the cluster. Only certain system catalogs (such as pg_database) are shared."));
    m_properties->addRow(tr("Persistence"), _record.value(_S("relpersistence")).toString(), tr("p = permanent table, u = unlogged table, t = temporary table"));
    m_properties->addRow(tr("Kind"), _record.value(_S("relkind")).toString(), tr("r = ordinary table, i = index, S = sequence, t = TOAST table, v = view, m = materialized view, c = composite type, f = foreign table, p = partitioned table"));
    m_properties->addRow(tr("Columns"), _record.value(_S("relnatts")).toString(), tr("Number of user columns in the relation (system columns not counted). There must be this many corresponding entries in pg_attribute. See also pg_attribute.attnum."));
    m_properties->addRow(tr("Check Constraints"), _record.value(_S("relchecks")).toString(), tr("Number of CHECK constraints on the table; see pg_constraint catalog"));
    m_properties->addRow(tr("Has OIDs?"), _record.value(_S("relhasoids")).toString(), tr("True if we generate an OID for each row of the relation"));
    m_properties->addRow(tr("Has Primary Key?"), _record.value(_S("relhaspkey")).toString(), tr("True if the table has (or once had) a primary key"));
    m_properties->addRow(tr("Has Rules?"), _record.value(_S("relhasrules")).toString(), tr("True if table has (or once had) rules; see pg_rewrite catalog"));
    m_properties->addRow(tr("Has Triggers?"), _record.value(_S("relhastriggers")).toString(), tr("True if table has (or once had) triggers; see pgtrigger catalog"));
    m_properties->addRow(tr("Is Inherited?"), _record.value(_S("relhassubclass")).toString(), tr("True if table has (or once had) any inheritance children"));
    m_properties->addRow(tr("Has Row-level Security?"), _record.value(_S("relrowsecurity")).toString(), tr("True if table has row level security enabled; see pg_policy catalog"));
    m_properties->addRow(tr("Row-level Security Applies to Owner?"), _record.value(_S("relforcerowsecurity")).toString(), tr("True if row level security (when enabled) will also apply to table owner; see pg_policy catalog"));
    m_properties->addRow(tr("Is Populated?"), _record.value(_S("relispopulated")).toString(), tr("True if relation is populated (this is true for all relations other than some materialized views)"));
    m_properties->addRow(tr("Replica Identity"), _record.value(_S("relreplident")).toString(), tr("Columns used to form “replica identity” for rows: d = default (primary key, if any), n = nothing, f = all columns i = index with indisreplident set, or default"));
    m_properties->addRow(tr("Is Partition?"), _record.value(_S("relispartition")).toString(), tr("True if table is a partition"));
    m_properties->addRow(tr("Partition Bound"), _record.value(_S("relpartbound")).toString(), tr("If table is a partition (see relispartition), internal representation of the partition bound"));
    m_properties->addRow(tr("Last Frozen Transaction Id"), _record.value(_S("relfrozenxid")).toString(), tr("All transaction IDs before this one have been replaced with a permanent (“frozen”) transaction ID in this table. This is used to track whether the table needs to be vacuumed in order to prevent transaction ID wraparound or to allow pg_xact to be shrunk. Zero (InvalidTransactionId) if the relation is not a table."));
    m_properties->addRow(tr("Minimum Multixact ID"), _record.value(_S("relminmxid")).toString(), tr("All multixact IDs before this one have been replaced by a transaction ID in this table. This is used to track whether the table needs to be vacuumed in order to prevent multixact ID wraparound or to allow pg_multixact to be shrunk. Zero (InvalidMultiXactId) if the relation is not a table."));
    m_properties->addRow(tr("ACL"), _record.value(_S("relacl")).toString(), tr("Access privileges; see GRANT and REVOKE for details"));
    m_properties->addRow(tr("Options"), _record.value(_S("reloptions")).toString(), tr("Access-method-specific options, as “keyword=value” strings"));
}

QVariant SQLItem::data(int column, int role, bool) const
{
    return TreeItem::data(column, role, true);
}

void SQLItem::getSQL()
{
    if (m_process && m_process->state() == QProcess::Running)
        return;

    if (!m_sql.isEmpty())
    {
        emit sql(m_sql);
        return;
    }

    auto si = findParent<SchemaItem>();
    assert(si);

    auto fullName = si->name() + _S(".") + title();
    QString typeStr, ddlStr;

    switch(type())
    {
        case itNULL:
        case itCatalog:
        case itDatabase:
        case itExtension:
        case itForeignDataWrapper:
        case itForeignTable:
        case itFunction:
        case itGenericSection:
        case itGroupRole:
        case itIndex:
        case itLanguage:
        case itLoginRole:
        case itNamespace:
        case itSchema:
        case itSequence:
        case itServer:
        case itServerGroup:
        case itServerGroups:
        case itTable:
        case itTableSpace:
        case itTrigger:
        case itTriggerFunction:
        case itType:
            goto PG_DUMP_FALLBACK;
        case itView:
        case itMaterializedView:
        {
            typeStr = _S("view");
            ddlStr = type() == itView ? _S("view") : _S("materialized view");
            break;
        }
    }

    {
        auto di = findParent<DatabaseItem>();
        assert(di);
        auto db = di->db;

        auto [success, query] = loadQuery(db, typeStr, _S("create"), {{_S(":oid"), oid()}});
        if (success && query.isActive())
        {
            if (query.next())
            {
                m_sql = _S("--This is still incomplete.\n\ncreate or replace %1 %2 as\n%3\n").
                            arg(ddlStr, fullName, query.value(0).toString());

                emit sql(m_sql);

                return;
            }
            else
            {
                assert(false);
            }
        }
        else // success && query.isActive()
            goto PG_DUMP_FALLBACK;
    }

PG_DUMP_FALLBACK:
    m_process = std::make_unique<QProcess>();

    connect(m_process.get(), QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            [this](int /*exitCode*/, QProcess::ExitStatus exitStatus)
            {
                if (exitStatus == QProcess::NormalExit)
                {
                    auto so = m_process->readAllStandardOutput();
                    auto se = m_process->readAllStandardError();

                    auto str = QString::fromLocal8Bit(so).split(_S("\n"));
                    auto match = _S("-- Name: %1;").arg(title());

                    while(str.size())
                    {
                        if (str[0].startsWith(match))
                            break;
                        str.erase(str.begin());
                    }

                    match = _S("--");

                    auto lastWasEmpty = false;

                    for(auto i = 0; i < str.size(); )
                    {
                        const auto &line = str[i];
                        if (line.startsWith(match))
                        {
                            lastWasEmpty = false;
                            str.erase(str.begin() + i);
                        }
                        else if (line.isEmpty())
                        {
                            if (lastWasEmpty)
                                str.erase(str.begin() + i);
                            else
                            {
                                i++;
                                lastWasEmpty = true;
                            }
                        }
                        else
                        {
                            lastWasEmpty = true;
                            i++;
                        }
                    }

                    m_sql = str.join(_S("\n")).trimmed();
                    m_process.reset();

                    emit sql(m_sql);
                }
            }
    );

    auto di = findParent<DatabaseItem>();
    m_process->start(_S("pg_dump"), {_S("-s"), _S("-d"), di->title(), _S("-o"), _S("-t"), fullName});
}

void SQLItem::loadStats()
{
    m_statsIsLoaded = true;
}
