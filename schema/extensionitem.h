#pragma once

#include "treeitem.h"

class ExtensionItem : public TreeItem
{
public:
    ExtensionItem(TreeItem *_parent, const QSqlRecord &_record);

    virtual ItemType type() const override { return itExtension; };
};
