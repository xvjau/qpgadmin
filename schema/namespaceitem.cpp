#include "namespaceitem.h"

NamespaceItem::NamespaceItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon):
    TreeItem(_parent, _record, _icon)
{
    setNoChildren();
}

QVariant NamespaceItem::data(int column, int role, bool) const
{
    return TreeItem::data(column, role, true);
}
