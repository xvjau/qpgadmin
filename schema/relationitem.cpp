#include "relationitem.h"
#include "statsmodel.h"
#include "databaseitem.h"

RelationItem::RelationItem(TreeItem *_parent, const QString &_title, const QString &_icon):
    SQLItem(_parent, _title, _icon)
{
}

RelationItem::RelationItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon):
    SQLItem(_parent, _record, _icon)
{
}

void RelationItem::loadStats()
{
    auto di = findParent<DatabaseItem>();
    assert(di);
    auto db = di->db;

    auto [success, query] = loadQuery(db, _S("table"), _S("stats"), {{_S(":oid"), oid()}});
    assert(success);

    m_stats = std::make_unique<StatsModel>(query);

    SQLItem::loadStats();
}

void RelationItem::getSQL()
{
    CursorGuard cg;
    if (!m_sql.isEmpty())
    {
        emit sql(m_sql);
        return;
    }

    auto di = findParent<DatabaseItem>();
    assert(di);
    auto db = di->db;
    auto hnd = di->handle();

    switch(type())
    {
        case itMaterializedView:
        case itView:
        {
            auto [success, query] = loadQuery(db, _S("view"), _S("create"), {{_S(":oid"), oid()}});
            assert(success);

            if(query.next())
            {
                auto schema = query.value(_S("nspname")).toString();
                auto name = query.value(_S("relname")).toString();
                auto kind = type() == itMaterializedView ? _S("materialized ") : QString();
                auto ddl = query.value(_S("pg_get_viewdef")).toString();

                m_sql = _S("create or replace %1view %2.%3 as\n%4").arg(kind,
                    escapeIdentifier(hnd, schema), escapeIdentifier(hnd, name), ddl);


                query.exec(_S(R"sql(select column_name, data_type, is_nullable
from "information_schema"."columns"
where table_schema = %1 and table_name = %2
order by ordinal_position)sql").arg(escpaeLiteral(hnd, schema),
                                    escpaeLiteral(hnd, name)));

                while(query.next())
                {
                    auto columnName = query.value(_S("column_name")).toString();
                    auto dataType = query.value(_S("data_type")).toString();
                    auto isNull = query.value(_S("is_nullable")).toString() == _S("YES");

                    m_fieldList.emplace_back(std::move(columnName), std::move(dataType),
                            QString(), isNull);
                }
            }
            break;
        }
        default:
        {
            auto [success, query] = loadQuery(db, _S("table"), _S("create"), {{_S(":oid"), oid()}});
            assert(success);

            if(query.next())
            {
                auto tableSchema = query.value(_S("table_schema")).toString();
                auto tableName = query.value(_S("table_name")).toString();
                m_fieldList.clear();
                m_sql = _S("create table %1.%2 (\n").arg(escapeIdentifier(hnd, tableSchema),
                                                        escapeIdentifier(hnd, tableName));

                do
                {
                    auto columnName = query.value(_S("column_name")).toString();
                    auto dataType = query.value(_S("data_type")).toString();
                    if(dataType.compare(_S("ARRAY"), Qt::CaseInsensitive) == 0)
                    {
                        dataType = query.value(_S("udt_name")).toString().mid(1) + _S("[]");
                    }
                    else if(dataType.compare(_S("USER-DEFINED"), Qt::CaseInsensitive) == 0)
                    {
                        dataType = query.value(_S("udt_name")).toString();
                    }
                    auto colDefault = query.value(_S("column_default")).toString();
                    if(!colDefault.isEmpty())
                    {
                        colDefault.insert(0, _S(" default "));
                    }
                    auto isNull = query.value(_S("is_nullable")).toString().compare(_S("YES"), Qt::CaseInsensitive) == 0;
                    auto nullable = isNull ? QString() : _S(" not null");

                    m_sql += _S("    %1 %2%3%4,\n").arg(escapeIdentifier(hnd, columnName), dataType, colDefault, nullable);

                    m_fieldList.emplace_back(std::move(columnName), std::move(dataType),
                                            std::move(colDefault), isNull);
                }
                while(query.next());

                m_sql.chop(2);
                m_sql += _S("\n)");
            }
        }
    }

    emit sql(m_sql);
};

RelationItem::Field::Field(QString &&_name, QString &&_type, QString &&_defaultValue, bool _nullable):
    name(_name), type(_type), defaultValue(_defaultValue), nullable(_nullable)
{
}
