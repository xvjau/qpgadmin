#include "catalogitem.h"

#include <cassert>

#include "schemaitem.h"
#include "databaseitem.h"
#include "genericsection.h"

CatalogItem::CatalogItem(TreeItem *_parent):
    TreeItem(_parent, tr("Catalogs"), _S("catalogs"))
{
}

void CatalogItem::load()
{
    if (state() == stUnloaded)
    {
        const auto count = 2;
        beginLoadChildren(count);

        auto di = findParent<DatabaseItem>();
        assert(di);
        auto db = di->db;

        auto ansi = make_child<SchemaItem>(_S("information_schema"), tr("information_schema"), _S("catalog"));
        ansi->setNoChildren();
        ansi->loadRelationObjects(db);
        ansi->sort();

        auto pg = make_child<SchemaItem>(_S("pg_catalog"), tr("Postgres"), _S("catalog"));
        pg->setNoChildren();
        pg->loadRelationObjects(db);
        pg->loadFunctions(db);
        pg->loadTypes(db);
        pg->sort();

        endLoadChildren();
        assert(childCount() == count);
    }
}
