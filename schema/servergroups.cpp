#include "servergroups.h"
#include "servergroup.h"
#include "serveritem.h"

#include <QUuid>
#include <QSettings>
#include <QMessageBox>
#include <unordered_map>

struct ServerKey
{
    QString HostAddr;
    int port;

    bool operator==(const ServerKey &_other) const
    {
        return port == _other.port &&
               HostAddr.compare(_other.HostAddr) == 0;
    }
};

namespace std {

template<>
struct hash<ServerKey>
{
    size_t
    operator()(const ServerKey &_key) const noexcept
    {
        auto h1 = hash<std::string>()(_key.HostAddr.toStdString());
        auto h2 = hash<int>()(_key.port);
        return h1 ^ (h2 << 1);
    }
};


} // namespace std

namespace {
const auto PGADMIN3_CONF = QDir::homePath() + QDir::separator() + _S(".pgadmin3");
}

ServerGroups::ServerGroups(TreeItem *_parent):
    TreeItem(_parent, tr("Server Groups"), _S("server"))
{
    load();
}

int ServerGroups::loadOldConfig()
{
    auto result = 0;
    auto file = QFile(PGADMIN3_CONF);

    if(file.exists() && file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        auto ret = QMessageBox::question(nullptr, tr("Load pqAdmin3"),
                                         tr("Load old pgAdmin3 config?"));

        if(ret == QMessageBox::Yes)
        {
            // 2-pass parse, to first get the count, then add the items
            for(int pass = 1; pass <= 2; pass++)
            {
                if(pass == 2)
                {
                    if(result == 0)
                        return 0;

                    // Now that we know the count, add the items
                    beginLoadChildren(result);
                }

                enum
                {
                    stRoot,
                    stServers,
                    stServerItem,
                } state = stRoot;
                auto sectionReg = QRegExp::escape(_S("[")) + _S("%1") + QRegExp::escape(_S("]"));
                auto serversReg = QRegExp(sectionReg.arg(_S("Servers")));
                auto serverItemReg = QRegExp(sectionReg.arg(_S("Servers/(\\d+)")));
                auto serverDatabaseReg = QRegExp(sectionReg.arg(_S("Servers/(\\d+)/Databases/(\\w+)")));
                QString Description, Server, HostAddr, Rolename, Group, Database, Username;
                int Port = 0;
                bool SSL = false;
                std::unordered_map<QString, ServerGroup *> groupMap;
                std::unordered_map<ServerKey, ServerItem *> serverMap;
                auto clearAll = [&]
                {
                    Description.clear();
                    Server.clear();
                    HostAddr.clear();
                    Rolename.clear();
                    Group.clear();
                    Database.clear();
                    Username.clear();
                    Port = 0;
                    SSL = false;
                };
                auto addServerItem = [&]
                {
                    if((!Description.isEmpty()) && (Port > 0) && (pass == 2))
                    {
                        //auto databaseName = serverDatabaseReg.cap(2);
                        auto db = QSqlDatabase::addDatabase(_S("QPSQL"), QUuid::createUuid().toString());

                        if(Server.isEmpty())
                            db.setHostName(HostAddr);
                        else
                            db.setHostName(Server);

                        db.setPort(Port);

                        if(Username.isEmpty())
                            db.setUserName(Rolename);
                        else
                            db.setUserName(Username);

                        auto options = _S("dbname=%1");

                        if(Database.isEmpty())
                            db.setConnectOptions(options.arg(_S("postgres")));
                        else
                            db.setConnectOptions(options.arg(Database));

                        auto si = serverMap[ {Description, Port}];

                        if(!si)
                        {
                            auto sg = groupMap[Group];

                            if(!sg)
                            {
                                sg = addGroup(Group);
                                groupMap[Group] = sg;
                            }

                            serverMap[ {Description, Port}] = sg->addServer(Description, db);
                        }
                    }

                    clearAll();
                };
                file.seek(0);

                while(!file.atEnd())
                {
                    auto line = QString::fromLocal8Bit(file.readLine()).trimmed();

                    if(line.isEmpty())
                        continue;

                    if(*line.begin() == _C('[') && *line.rbegin() == _C(']'))
                    {
                        if(serversReg.exactMatch(line))
                            state = stServers;
                        else if(serverItemReg.exactMatch(line))
                        {
                            // Already in this state, so we just reached a new entry.
                            if(state == stServerItem)
                                addServerItem();
                            else
                                state = stServerItem;
                        }
                        else if(serverDatabaseReg.exactMatch(line))
                        {
                            if((!Description.isEmpty()) && (Port > 0))
                            {
                                if(pass == 1)
                                    result++;
                                else
                                {
                                    addServerItem();
                                }
                            }

                            clearAll();
                        }

                        continue;
                    }

                    switch(state)
                    {
                        case stRoot:
                        {
                            break;
                        }

                        case stServers:
                        {
                            break;
                        }

                        case stServerItem:
                        {
                            auto pos = line.indexOf(_C('='));

                            if(pos < 0)
                                continue;

                            auto key = line.left(pos);
                            auto value = line.mid(pos + 1);

                            if(key == _S("Server"))
                                Server = value;
                            else if(key == _S("Description"))
                                Description = value;
                            else if(key == _S("Database"))
                                Database = value;
                            else if(key == _S("HostAddr"))
                                HostAddr = value;
                            else if(key == _S("Port"))
                                Port = value.toInt();
                            else if(key == _S("Rolename"))
                                Rolename = value;
                            else if(key == _S("Group"))
                                Group = value;
                            else if(key == _S("Username"))
                                Username = value;

                            break;
                        }
                    }
                }

                addServerItem();
                assert(result > 0);
            }
        }
    }

    return result;
}

void ServerGroups::load()
{
    if(state() == stUnloaded)
    {
        QSettings settings;
        auto count = settings.beginReadArray(_S("groups"));

        if(count == 0)
        {
            count = loadOldConfig();

            if(childCount() < 1)
            {
                count = 1;
                beginLoadChildren(count);
                addItem(tr("Servers"));
            }
        }
        else
        {
            beginLoadChildren(count);

            for(int i = 0; i < count; i++)
            {
                settings.setArrayIndex(i);
                addItem(settings.value(_S("name")).toString());
            }
        }

        settings.endArray();
        sort();
        endLoadChildren();
    }
}

ServerGroup *ServerGroups::addItem(const QString &name)
{
    auto item = make_child<ServerGroup>(name);
    return item;
}

ServerGroup *ServerGroups::addGroup(const QString &name)
{
    auto result = addItem(name);
    QSettings settings;
    settings.beginWriteArray(_S("groups"));
    auto count = childCount();
    settings.setArrayIndex(count - 1);
    settings.setValue(_S("name"), name);
    settings.endArray();
    // Sort can only be called in load(), unless we emit rowMoved signals
    //sort();
    return result;
}
