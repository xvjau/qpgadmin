#pragma once

#include "treeitem.h"

class LanguageItem : public TreeItem
{
public:
    LanguageItem(TreeItem *_parent, const QSqlRecord &_record);

    virtual ItemType type() const override { return itLanguage; }
};
