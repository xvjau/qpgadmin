#pragma once

#include "genericsection.h"
#include "triggeritem.h"

class TriggersSection : public GenericSection
{
public:
    TriggersSection(TreeItem *_parent);

    TriggerItem* addItem(const QSqlRecord &_record, const QString &_signature, const QString &_sql)
    {
        return make_child<TriggerItem>(_record, _signature, _sql);
    }
};
