#pragma once

#include <QAbstractItemModel>
#include <QSqlDatabase>
#include <memory>

struct TreeItem;
typedef std::shared_ptr<TreeItem> TreeItemPtr;

class PGItemModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    PGItemModel();
    PGItemModel(QSqlDatabase &db);

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    void loadItem(const QModelIndex &index);

    QModelIndex indexOf(TreeItem *_item);
#ifndef NDEBUG
    QModelIndex findItem(int _type, const QString &_title, const QModelIndex &_parent = QModelIndex());
#endif

public slots:
    void addGroup(QString name);

private:
    TreeItemPtr m_rootItem;

signals:
    void itemUnloaded(const QModelIndex &index);

private slots:
    void onTreeItemBeginLoad(TreeItem *item, int _count);
    void onTreeItemEndLoad();

    void onTreeItemBeginUnLoad(TreeItem *item, int _count);
    void onTreeItemEndUnLoad();
};
