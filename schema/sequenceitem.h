#pragma once

#include "sqlitem.h"

class SequenceItem : public SQLItem
{
public:
    SequenceItem(TreeItem *_parent, const QSqlRecord &_record);

    virtual ItemType type() const override
    {
        return itSequence;
    }
    virtual void getSQL() override;
};
