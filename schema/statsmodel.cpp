#include "statsmodel.h"

#include <QSqlRecord>

StatsModel::StatsModel()
{
}

StatsModel::StatsModel(QSqlQuery _query)
{
    _query.first();
    auto rec = _query.record();
    auto count = rec.count();
    for(int i = 0; i < count; i++)
    {
        addRow(rec.fieldName(i), rec.value(i));
    }
}

void StatsModel::addRow(const QString &_field, const QVariant &_value, const QString &_comment )
{
    m_rows.push_back({_field, _value, _comment});
}

QVariant StatsModel::data(const QModelIndex& index, int role) const
{
    switch(role)
    {
        case Qt::DisplayRole:
        {
            switch(index.column())
            {
                case 0: return m_rows[index.row()].field;
                case 1: return m_rows[index.row()].value;
            }
            break;
        }
        case Qt::ToolTip:
        {
            const auto& comment = m_rows[index.row()].comment;
            if (!comment.isEmpty())
                return comment;
            break;
        }
    }
    return QVariant();
}

QVariant StatsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    switch(role)
    {
        case Qt::DisplayRole:
        {
            switch(orientation)
            {
                case Qt::Horizontal:
                {
                    switch(section)
                    {
                        case 0: return tr("Statistic");
                        case 1: return tr("Value");
                    }
                    break;
                }
                case Qt::Vertical:
                {
                    return section;
                }
            }
            break;
        }
    }
    return QVariant();
}

int StatsModel::columnCount(const QModelIndex&) const
{
    return 2;
}

int StatsModel::rowCount(const QModelIndex&) const
{
    return m_rows.size();
}
