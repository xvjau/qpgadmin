#pragma once

#include "treeitem.h"

class GroupRolesItem : public TreeItem
{
public:
    GroupRolesItem(TreeItem *_parent, const QSqlRecord &_record);

    virtual QVariant data(int column, int role, bool) const;

    virtual ItemType type() const override { return itGroupRole; }
};
