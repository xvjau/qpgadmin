#pragma once

#include <QSyntaxHighlighter>
#include <QRegularExpression>
#include <QTextCharFormat>
#include <QVector>

class QTextDocument;

class SQLSyntaxHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    SQLSyntaxHighlighter(QSqlDatabase &_db, QTextDocument *parent = 0);

protected:
    void highlightBlock(const QString &text) override;

private:
    struct HighlightingRule
    {
        QRegularExpression pattern;
        QTextCharFormat format;
    };
    QVector<HighlightingRule> highlightingRules;

    //QRegularExpression commentStartExpression;
    //QRegularExpression commentEndExpression;

    QTextCharFormat commentFormat;
    QTextCharFormat keywordFormat;
    //     QTextCharFormat classFormat;
    QTextCharFormat singleLineCommentFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat functionFormat;
    QTextCharFormat castFormat;
    QTextCharFormat numberFormat;
};
