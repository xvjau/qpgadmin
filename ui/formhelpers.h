/*
 * <B1tF0rge> 2014 Todos os direitos reservados
 */

#pragma once

#include <queue>
#include <initializer_list>
#include <QtCore/QTimer>
#include <QtCore/QDateTime>
#include <QtWidgets>

class QTableView;
class QLineEdit;
class QSplitter;

void setAllTablesViewState(QWidget *parent);
void setTableViewState(QTableView *tv);

void saveWidgetState(const QWidget *widget);
void saveAllTablesViewState(const QWidget *parent);
void saveTableViewState(const QTableView *tv);

void setWidgetState(QWidget *widget);
void setAllSplitersState(QWidget *parent);
void setSpliterState(QSplitter *splitter);

void saveAllSplitersState(QWidget *parent);
void saveSpliterState(QSplitter *splitter);

void enableActionsInCurrentWindow(QWidget *window, std::initializer_list<QAction *> _acActions);
