#pragma once

#include <QMainWindow>
#include <QSqlDatabase>
#include <QModelIndex>

#include <memory>
#include <tuple>

namespace Ui {
class Query;
}

class MainWindow;

QT_BEGIN_NAMESPACE
class QSqlQueryModel;
class QTextEdit;
QT_END_NAMESPACE

class Query : public QMainWindow
{
    Q_OBJECT
private:
    Ui::Query *m_ui = nullptr;

    QSqlDatabase m_db;

    bool m_isPrepared = false;
    std::shared_ptr<QSqlQueryModel> m_model;

    void doExec(const QString &_sql = QString());

    void execute();
    void executeText(QTextEdit *_teSQL);

    void prepare();
    void prepareText(QTextEdit *_teSQL);
    void clearPrepared();

    void open();
    std::tuple<int, QTextEdit *> newQuery();
    void save();
    void saveTemp();

public:
    Query(MainWindow *parent, QSqlDatabase db, QStringView title);
    virtual ~Query();

public slots:
    void createdEvent();
    void executeQuery(const QString &_sql);

protected:
    void closeEvent(QCloseEvent *event) override;
};

