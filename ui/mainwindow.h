#pragma once

#include <QMainWindow>
#include <QSqlDatabase>
#include <QModelIndex>

#include <QMenu>

namespace Ui {
class MainWindow;
}

class PGItemModel;
class TreeItem;
class DatabaseItem;
class Query;

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    Ui::MainWindow *m_ui = nullptr;
    PGItemModel *m_itemModel = nullptr;

    QSqlDatabase m_db;

    TreeItem *m_currentItem = nullptr;
    DatabaseItem *m_currentDatabase = nullptr;

    QMenu m_menuServerGroup;
    QMenu m_menuServer;
    QMenu m_menuDatabase;
    QMenu m_menuFunction;
    QMenu m_menuRelation;
    QMenu m_menuLogin;
    QMenu m_menuRoles;

    QMetaObject::Connection m_teSqlConnection_sql;

    void setupMenusAndActions();

    void itemActivated(const QModelIndex &index);
    void itemDoubleClicked(const QModelIndex &index);
    void onCustomContextMenu(const QPoint &point);
    void actionQuery();

    void onAddServerGroup();
    void onAddServer();
    void onDeleteServer();
    void onDisconnectServer();
    void onDisconnectDatabase();
    void onViewData();

public:
    MainWindow();
    MainWindow(QSqlDatabase db);
    virtual ~MainWindow();

protected:
    Query *makeQuery();
    void closeEvent(QCloseEvent *event) override;

#ifndef NDEBUG
    // Unit tests
public:
    void test_selectTreeItem();
#endif
};

