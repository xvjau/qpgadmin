#include "query.h"
#include "ui_query.h"
#include "mainwindow.h"
#include "formhelpers.h"
#include "schema/pgitemmodel.h"
#include "schema/sqlitem.h"
#include "sqlsyntaxhighlighter.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlQueryModel>
#include <QDir>
#include <QFileDialog>

Query::Query(MainWindow *parent, QSqlDatabase db, QStringView title) :
    QMainWindow(parent),
    m_db(db)
{
    setAttribute(Qt::WA_DeleteOnClose);
    m_ui = new Ui::Query();
    m_ui->setupUi(this);

    if(!title.isEmpty())
        setWindowTitle(title.toString());

    auto twQueries = m_ui->twQueries;

    while(twQueries->count())
        twQueries->removeTab(0);

    connect(m_ui->actionExecuteQuery, &QAction::triggered, this, &Query::execute);
    connect(m_ui->actionPrepareQuery, &QAction::triggered, this, &Query::prepare);
    m_ui->actionExecuteQuery->setShortcut(tr("Ctrl+Return"));
    setWidgetState(this);
    setAllSplitersState(this);
    connect(m_ui->actionOpen, &QAction::triggered, this, &Query::open);
    connect(m_ui->actionSave, &QAction::triggered, this, &Query::save);
    connect(m_ui->actionNew, &QAction::triggered, this, &Query::newQuery);
    QSettings settings;
    auto count = settings.beginReadArray(_S("queries"));

    if(count)
    {
        for(auto i = 0; i < count; i++)
        {
            settings.setArrayIndex(i);
            auto te = std::get<1>(newQuery());
            assert(te);

            if(te)
            {
                te->setPlainText(settings.value(_S("query")).toString());
            }
        }
    }
    else
    {
        newQuery();
    }

    QMetaObject::invokeMethod(this, &Query::createdEvent);
}

Query::~Query()
{
    delete m_ui;
}

void setHighlighter(QSqlDatabase &db, QObject *o)
{
    for(auto c : o->children())
    {
        auto te = qobject_cast<QTextEdit *>(c);

        if(te)
        {
            DEBUG_MSG("Set Highlighter for :" << te->objectName() << "\n");
            new SQLSyntaxHighlighter(db, te->document());
        }
        else
        {
            setHighlighter(db, c);
        }
    }
}

void Query::createdEvent()
{
    setHighlighter(m_db, this);
    DEBUG_MSG(std::flush);
}

void Query::closeEvent(QCloseEvent *event)
{
    QSettings settings;
    settings.remove(_S("queries"));
    saveAllSplitersState(this);
    saveWidgetState(this);
    QMainWindow::closeEvent(event);
}

void Query::execute()
{
    saveTemp();

    if(m_model && m_isPrepared)
    {
        auto boundValues = m_model->query().boundValues();
        auto twVariables = m_ui->twVariables;
        auto it = boundValues.begin(), end = boundValues.end();
        auto row = 0;

        for(; it != end; it++, row++)
        {
            m_model->query().bindValue(row, twVariables->item(row, 1)->data(Qt::DisplayRole));
        }

        doExec();
    }
    else
    {
        auto twQueries = m_ui->twQueries;
        auto teSQL = twQueries->currentWidget()->findChild<QTextEdit *>();
        executeText(teSQL);
    }
}

template<typename T>
QString getDurantion(const std::chrono::time_point<T> &_start)
{
    auto duration = (double)std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _start).count();

    if(duration < 1000)
    {
        return _S("%1ms").arg(duration);
    }

    duration /= 1000;

    if(duration < 60)
    {
        duration = round(duration * 100) / 100;
        return _S("%1s").arg(duration);
    }

    auto min = QString::number(duration / 60);
    auto sec = ((int)duration) % 60;
    return _S("%1:%2").arg(min).arg(sec, 2, _C('0'));
}

void Query::executeText(QTextEdit *_teSQL)
{
    CursorGuard cg;
    m_model = std::make_shared<QSqlQueryModel>();
    clearPrepared();
    QString sql;

    if(_teSQL->textCursor().hasSelection())
        sql = _teSQL->textCursor().selection().toPlainText();
    else
        sql = _teSQL->toPlainText();

    doExec(sql);
}

void Query::clearPrepared()
{
    m_isPrepared = false;
    m_ui->twVariables->clear();
}

void Query::prepare()
{
    saveTemp();
    auto twQueries = m_ui->twQueries;
    auto teSQL = twQueries->currentWidget()->findChild<QTextEdit *>();
    prepareText(teSQL);
}

void Query::prepareText(QTextEdit *_teSQL)
{
    CursorGuard cg;
    m_model = std::make_shared<QSqlQueryModel>();
    QString sql;

    if(_teSQL->textCursor().hasSelection())
        sql = _teSQL->textCursor().selection().toPlainText();
    else
        sql = _teSQL->toPlainText();

    auto query = QSqlQuery(m_db);
    clearPrepared();
    m_isPrepared = query.prepare(sql);
    auto boundValues = query.boundValues();
    m_model->setQuery(query);

    if(m_isPrepared)
    {
        auto twOutput = m_ui->twOutput;
        auto twVariables = m_ui->twVariables;
        twVariables->setRowCount(boundValues.count());
        twVariables->setColumnCount(2);
        twVariables->setHorizontalHeaderLabels({tr("Variable"), tr("Value")});
        auto it = boundValues.begin(), end = boundValues.end();
        auto row = 0;

        for(; it != end; it++, row++)
        {
            auto newKey = new QTableWidgetItem(it.key());
            //newKey->setFlags(newKey->flags() | Qt::ItemIsEditable);
            twVariables->setItem(row, 0, newKey);
            auto newValue = new QTableWidgetItem(it.value().toString());
            newValue->setFlags(newValue->flags() | Qt::ItemIsEditable);
            twVariables->setItem(row, 1, newValue);
        }

        twOutput->setCurrentIndex(4);
    }
}

void Query::doExec(const QString &_sql)
{
    auto twOutput = m_ui->twOutput;
    auto teMessages = m_ui->teMessages;
    auto teHistory = m_ui->teHistory;
    auto tvDataOutput = m_ui->tvDataOutput;
    auto start = std::chrono::steady_clock::now();

    if(_sql.isEmpty())
        m_model->query().exec();
    else
        m_model->setQuery(_sql, m_db);

    auto sql = m_model->query().lastQuery();
    auto err = m_model->query().lastError();

    if(err.isValid())
    {
        twOutput->setCurrentIndex(2);
        teMessages->setPlainText(err.databaseText());
        tvDataOutput->setModel(nullptr);
        auto cursor = QTextCursor(teHistory->document());
        cursor.movePosition(QTextCursor::End);
        cursor.insertText(_sql);
        cursor.insertText(tr("\n-- Error:\n%2\n\n").arg(err.databaseText()));
    }
    else
    {
        auto duration = getDurantion(start);
        auto message = tr("Query:\n\n%1\n\nExecution time: %2").arg(sql, duration);
        teMessages->setPlainText(message);
        auto cursor = QTextCursor(teHistory->document());
        cursor.movePosition(QTextCursor::End);
        cursor.insertText(_sql);
        cursor.insertText(tr("\n-- Execution time: %1\n\n").arg(duration));
        teMessages->ensureCursorVisible();
        tvDataOutput->setModel(m_model.get());

        if(m_model->columnCount())
            twOutput->setCurrentIndex(0);
        else
            twOutput->setCurrentIndex(2);
    }
}

void Query::open()
{
    QSettings settings;
    settings.beginGroup(m_db.databaseName());
    auto dir = settings.value(_S("dir"), QDir::homePath()).toString();
    auto twQueries = m_ui->twQueries;
    auto files = QFileDialog::getOpenFileNames(this, tr("Open Query"), dir, tr("SQL Files (*.sql);;All files (*.*)"));

    for(auto it : files)
    {
        auto file = std::make_unique<QFile>(it);
        auto info = QFileInfo(file->fileName());

        if(!file->open(QIODevice::ReadOnly))
        {
            QMessageBox::critical(this, tr("File Error"), tr("Open failed: '%1'<br>%2")
                                  .arg(info.fileName()), file->errorString());
            continue;
        }

        auto fileData = QString::fromUtf8(file->readAll());

        if(file->error())
        {
            QMessageBox::critical(this, tr("File Error"), tr("Read failed: '%1'<br>%2")
                                  .arg(info.fileName()), file->errorString());
            continue;
        }

        file->close();
        int idx;
        QTextEdit *sql;
        {
            auto currentWidget = twQueries->currentWidget()->findChild<QTextEdit *>();
            auto currentIndex = twQueries->currentIndex();

            if(currentWidget->document()->isEmpty())
            {
                idx = currentIndex;
                sql = currentWidget;
            }
            else
                std::tie(idx, sql) = newQuery();
        }
        twQueries->setTabText(idx, info.fileName());
        sql->setText(fileData);
        file->setParent(sql);
        file.release();
        auto dirName = info.dir().path();

        if(dirName != dir)
        {
            settings.setValue(_S("dir"), dirName);
            dir = dirName;
        }
    }
}

std::tuple<int, QTextEdit *> Query::newQuery()
{
    auto twQueries = m_ui->twQueries;
    auto num = twQueries->count() + 1;
    auto tab = new QWidget();
    tab->setObjectName(_S("tbQuery_%1").arg(num));
    auto gl = new QGridLayout(tab);
    auto te = new QTextEdit(tab);
    te->setObjectName(_S("teSQL_%1").arg(num));
    new SQLSyntaxHighlighter(m_db, te->document());
    gl->addWidget(te, 0, 0, 1, 1);
    auto idx = twQueries->addTab(tab, tr("Query %1").arg(num));
    twQueries->setCurrentIndex(idx);
    te->setFocus();
    return {idx, te};
}

void Query::save()
{
    QSettings settings;
    settings.beginGroup(m_db.databaseName());
    auto dir = settings.value(_S("dir"), QDir::homePath()).toString();
    auto twQueries = m_ui->twQueries;
    auto teSQL = twQueries->currentWidget()->findChild<QTextEdit *>();
    auto idx = twQueries->currentIndex();
    auto file = teSQL->findChild<QFile *>();

    if(file)
        dir = file->fileName();

    auto fileName = QFileDialog::getSaveFileName(this, tr("Save Query"), dir, tr("SQL Files (*.sql);;All files (*.*)"));

    if(!fileName.isEmpty())
    {
        if((!file) || (fileName != file->fileName()))
        {
            delete file;
            file = new QFile(fileName);
            file->setParent(teSQL);
        }

        auto info = QFileInfo(fileName);

        if(!file->open(QIODevice::Truncate | QIODevice::WriteOnly))
        {
            QMessageBox::critical(this, tr("File Error"), tr("Open failed: '%1'<br>%2")
                                  .arg(info.fileName()), file->errorString());
            return;
        }

        auto data = teSQL->document()->toPlainText().toUtf8();

        if(file->write(data) < data.size())
        {
            QMessageBox::critical(this, tr("File Error"), tr("Write failed: '%1'<br>%2")
                                  .arg(info.fileName()), file->errorString());
            return;
        }

        file->close();
        twQueries->setTabText(idx, info.fileName());
    }
}

void Query::executeQuery(const QString &_sql)
{
    auto twQueries = m_ui->twQueries;
    auto teSQL = twQueries->currentWidget()->findChild<QTextEdit *>();
    assert(teSQL);
    teSQL->setText(_sql);
    executeText(teSQL);
}

void Query::saveTemp()
{
    auto twQueries = m_ui->twQueries;
    auto count = twQueries->count();
    QSettings settings;
    settings.beginWriteArray(_S("queries"), count);

    for(auto i = 0; i < count; i++)
    {
        settings.setArrayIndex(i);
        auto w = twQueries->widget(i);
        auto te = w->findChild<QTextEdit *>(_S("teSQL_%1").arg(i + 1));
        assert(te);

        if(te)
        {
            settings.setValue(_S("query"), te->toPlainText());
        }
    }
}
