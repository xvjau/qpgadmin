#include "qtooltipper.h"

QToolTipper::QToolTipper(QObject *parent) :
    QObject(parent)
{
}

bool QToolTipper::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::ToolTip)
    {
        auto view = static_cast<QAbstractItemView*>(obj->parent());
        if (!view)
        {
            return false;
        }

        auto helpEvent = static_cast<QHelpEvent*>(event);
        auto pos = helpEvent->pos();
        auto index = view->indexAt(pos);
        if (!index.isValid())
        {
            return false;
        }

        //auto itemText = view->model()->data(index).toString();
        auto itemTooltip = view->model()->data(index, Qt::ToolTipRole).toString();

        //auto fm(view->font());
        //auto itemTextWidth = fm.width(itemText);
        auto rect = view->visualRect(index);
        //auto rectWidth = rect.width();

        if (!itemTooltip.isEmpty())
        {
            QToolTip::showText(helpEvent->globalPos(), itemTooltip, view, rect);
        }
        else
        {
            QToolTip::hideText();
        }
        return true;
    }
    return false;
}

// kate: indent-mode cstyle; indent-width 4; replace-tabs on;
