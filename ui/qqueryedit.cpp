#include "qqueryedit.h"
#include <QtCore/QMimeData>
#include <QtGui/QFontDatabase>
#include "main.h"

QQueryEdit::QQueryEdit(QWidget* parent):
    QTextEdit(parent)
{
    // Run init after this object is materialised
    QMetaObject::invokeMethod(this, "init", Qt::QueuedConnection);
}

QQueryEdit::QQueryEdit(const QString& text, QWidget* parent):
    QTextEdit(text, parent)
{
    // Run init after this object is materialised
    QMetaObject::invokeMethod(this, "init", Qt::QueuedConnection);
}

void QQueryEdit::init()
{
    auto fontdb = QFontDatabase();
    auto fam = fontdb.families();

    auto font = document()->defaultFont();

    for(const auto& f : {_S("Monospace"), _S("DejaVu Sans Mono"), _S("Noto Sans Mono")})
    {
        if(fam.contains(f))
        {
            DEBUG_MSG("Set font to: " << f << "\n");
            font.setFamily(f);
            document()->setDefaultFont(font);
            break;
        }
    }

    auto rect = QFontMetrics(font).boundingRect(_S("    "));
    setTabStopDistance(rect.width());
    setAcceptRichText(false);
}

void QQueryEdit::insertFromMimeData(const QMimeData* source)
{
    for(const auto& data : source->formats())
        DEBUG_MSG(data << "\n");
    DEBUG_MSG(endl);

    QTextEdit::insertFromMimeData(source);
}
