#pragma once

#include <QTextEdit>

/**
 * @todo write docs
 */
class QQueryEdit : public QTextEdit
{
    Q_OBJECT

public:
    /**
     * Constructor
     *
     * @param parent TODO
     */
    QQueryEdit(QWidget* parent);

    /**
     * Constructor
     *
     * @param text TODO
     * @param parent TODO
     */
    QQueryEdit(const QString& text, QWidget* parent);

protected:
    /**
     * @todo write docs
     *
     * @param source TODO
     * @return TODO
     */
    virtual void insertFromMimeData(const QMimeData* source) override;

protected slots:
    void init();
};
