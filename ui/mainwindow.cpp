#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "formhelpers.h"
#include "qtooltipper.h"
#include "query.h"
#include "schema/databaseitem.h"
#include "schema/pgitemmodel.h"
#include "schema/relationitem.h"
#include "schema/schemaitem.h"
#include "schema/servergroup.h"
#include "schema/servergroups.h"
#include "schema/serveritem.h"
#include "schema/sqlitem.h"
#include "server.h"
#include "sqlsyntaxhighlighter.h"

#include <QSqlDriver>
#include <QSqlError>
#include <QUuid>

#ifndef NDEBUG
#include <QAbstractItemModelTester>
#endif

#include <QInputDialog>
#include <QMessageBox>

MainWindow::MainWindow():
    MainWindow(QSqlDatabase())
{
}

MainWindow::MainWindow(QSqlDatabase db) :
    QMainWindow(nullptr),
    m_db(db)
{
    setAttribute(Qt::WA_DeleteOnClose);
    m_ui = new Ui::MainWindow();
    m_ui->setupUi(this);

    setupMenusAndActions();

    auto tvObjectBrowser = m_ui->tvObjectBrowser;
    auto tvDependencies = m_ui->tvDependencies;
    auto tvDependents = m_ui->tvDependents;
    auto tvProperties = m_ui->tvProperties;
    auto tvStatistics = m_ui->tvStatistics;

    for(auto tv : {tvDependencies, tvDependents, tvProperties, tvStatistics})
    {
        tv->viewport()->installEventFilter(new QToolTipper(tv));
    }
    tvObjectBrowser->viewport()->installEventFilter(new QToolTipper(tvObjectBrowser));

    if (m_db.isOpen())
        m_itemModel = new PGItemModel(m_db);
    else
        m_itemModel = new PGItemModel();

    tvObjectBrowser->setModel(m_itemModel);
    tvObjectBrowser->setHeaderHidden(true);
    tvObjectBrowser->setContextMenuPolicy(Qt::CustomContextMenu);
    tvObjectBrowser->expandToDepth(0);

    connect(tvObjectBrowser, &QAbstractItemView::clicked, this, &MainWindow::itemActivated);
    connect(tvObjectBrowser, &QAbstractItemView::doubleClicked, this, &MainWindow::itemDoubleClicked);
    connect(tvObjectBrowser, &QAbstractItemView::customContextMenuRequested, this, &MainWindow::onCustomContextMenu);
    connect(m_itemModel, &PGItemModel::itemUnloaded, tvObjectBrowser, &QTreeView::collapse);

    auto actionSQL = m_ui->actionSQL;

    connect(actionSQL, &QAction::triggered, this, &MainWindow::actionQuery);
    actionSQL->setEnabled(false);

    setWidgetState(this);
    setAllSplitersState(this);

#ifndef NDEBUG
    QAbstractItemModelTester tester(m_itemModel, QAbstractItemModelTester::FailureReportingMode::Fatal, this);
#endif
}

void MainWindow::setupMenusAndActions()
{
    auto actionNew_Group = m_ui->actionNew_Group;
    auto actionNew_Server = m_ui->actionNew_Server;
    auto actionDelete_Server = m_ui->actionDelete_Server;
    auto actionDisconnect_Server = m_ui->actionDisconnect_Server;
    auto actionDisconnect_Database = m_ui->actionDisconnect_Database;
    auto actionView_Data = m_ui->actionView_Data;

    auto setupMenu = [this](QMenu &menu, std::initializer_list<QAction*> actions)
    {
        menu.clear();
        for(auto a : actions)
            menu.addAction(a);
    };

    setupMenu(m_menuServerGroup,    {actionNew_Group,});
    setupMenu(m_menuServer,         {actionNew_Server, actionDelete_Server, actionDisconnect_Server,});
    setupMenu(m_menuDatabase,       {actionDisconnect_Database});

#define setupAction(action, fn) \
    do { action->setEnabled(true); auto ret = connect(action, &QAction::triggered, this, fn); assert(ret); (void)(ret); } while (false)

    setupAction(actionNew_Group, &MainWindow::onAddServerGroup);
    setupAction(actionNew_Server, &MainWindow::onAddServer);
    setupAction(actionDelete_Server, &MainWindow::onDeleteServer);
    setupAction(actionDisconnect_Server, &MainWindow::onDisconnectServer);
    setupAction(actionDisconnect_Database, &MainWindow::onDisconnectDatabase);
    setupAction(actionView_Data, &MainWindow::onViewData);

    actionNew_Group->setEnabled(true);
}

MainWindow::~MainWindow()
{
    delete m_itemModel;
    delete m_ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    saveAllSplitersState(this);
    saveWidgetState(this);
    QMainWindow::closeEvent(event);
}

void MainWindow::itemDoubleClicked(const QModelIndex &index)
{
    m_itemModel->loadItem(index);
}

void MainWindow::itemActivated(const QModelIndex &index)
{
    auto setSQLPane = [this]()
    {
        auto ptr = dynamic_cast<SQLItem*>(m_currentItem);
        if (ptr)
        {
            auto db = ptr->findParent<DatabaseItem>()->db;
            auto teSQLPane = m_ui->teSQLPane;

            teSQLPane->disconnect(m_teSqlConnection_sql);
            teSQLPane->clear();
            m_teSqlConnection_sql = connect(ptr, &SQLItem::sql, teSQLPane, &QTextEdit::setText);
            ptr->getSQL();

            teSQLPane->setFontFamily(_S("monospace"));
            new SQLSyntaxHighlighter(db, teSQLPane->document());

            auto tabWidget = m_ui->tabWidget;
            auto tvProperties = m_ui->tvProperties;
            auto tvStatistics = m_ui->tvStatistics;
            auto tvDependencies = m_ui->tvDependencies;
            auto tvDependents = m_ui->tvDependents;

            saveAllTablesViewState(tabWidget);

            tvProperties->setModel(ptr->properties());
            tvStatistics->setModel(ptr->stats());
            tvDependencies->setModel(ptr->dependencies());
            tvDependents->setModel(ptr->dependents());

            setAllTablesViewState(tabWidget);
        }
    };

    auto setCurrentDatabaseActions = [this]()
    {
        m_currentDatabase = m_currentItem->findParent<DatabaseItem>();
        if (m_currentDatabase)
        {
            auto isOpen = m_currentDatabase->db.isOpen();
            m_ui->actionSQL->setEnabled(isOpen);
            m_ui->actionDisconnect_Database->setEnabled(isOpen);
        }
        else
        {
            m_ui->actionSQL->setEnabled(false);
            m_ui->actionDisconnect_Database->setEnabled(false);
        }
    };

    m_currentItem = static_cast<TreeItem*>(index.internalPointer());
    if (m_currentItem)
    {
        setCurrentDatabaseActions();

        switch(m_currentItem->type())
        {
            case TreeItem::itServerGroups:
            case TreeItem::itServerGroup:
                m_ui->actionDisconnect_Server->setEnabled(false);
                m_ui->actionDelete_Server->setEnabled(false);
                break;
            case TreeItem::itServer:
                m_ui->actionDisconnect_Server->setEnabled(true);
                m_ui->actionDelete_Server->setEnabled(true);
                break;

            case TreeItem::itNULL:
            case TreeItem::itCatalog:
            case TreeItem::itGenericSection:
            case TreeItem::itLanguage:
            case TreeItem::itNamespace:
            case TreeItem::itSchema:
            case TreeItem::itTableSpace:
            case TreeItem::itExtension:
            case TreeItem::itDatabase:
            case TreeItem::itForeignDataWrapper:
            case TreeItem::itForeignTable:
            case TreeItem::itFunction:
            case TreeItem::itGroupRole:
            case TreeItem::itIndex:
            case TreeItem::itLoginRole:
            case TreeItem::itSequence:
            case TreeItem::itTable:
            case TreeItem::itTrigger:
            case TreeItem::itTriggerFunction:
            case TreeItem::itType:
            case TreeItem::itView:
            case TreeItem::itMaterializedView:
            {
                m_ui->actionDisconnect_Server->setEnabled(true);
                m_ui->actionDelete_Server->setEnabled(false);

                setSQLPane();
                break;
            }
        }
    }
}

void MainWindow::onCustomContextMenu(const QPoint &point)
{
    auto tvObjectBrowser = m_ui->tvObjectBrowser;

    auto index = tvObjectBrowser->indexAt(point);
    if (index.isValid())
    {
        if (m_currentDatabase)
            m_menuDatabase.popup(tvObjectBrowser->viewport()->mapToGlobal(point));
        else if (dynamic_cast<ServerGroup*>(m_currentItem))
            m_menuServer.popup(tvObjectBrowser->viewport()->mapToGlobal(point));
    }
    else
        m_menuServerGroup.popup(tvObjectBrowser->viewport()->mapToGlobal(point));
}

Query* MainWindow::makeQuery()
{
    Query* result;
    if(m_currentDatabase)
    {
        auto db = m_currentDatabase;
        auto title = db->findParent<ServerItem>()->title() + _S(" - ") + db->title();
        result = new Query(this, db->db, title);
    }
    else
    {
        result = new Query(this, m_db, QString());
    }
    result->show();
    return result;
}

void MainWindow::actionQuery()
{
    makeQuery();
}

void MainWindow::onAddServerGroup()
{
    auto getName = [this]
    {
        bool ok = false;
        auto result = QInputDialog::getText(this, tr("Server Group"), tr("Group name:"), QLineEdit::Normal, QString(), &ok);
        return std::make_pair(ok, result);
    };

    if (m_currentItem)
    {
        auto sg = m_currentItem->findParent<ServerGroups>();
        if (sg)
        {
            auto [ok, name] = getName();
            if (ok && !name.isEmpty())
                sg->addGroup(name);

            return;
        }
    }

    auto [ok, name] = getName();
    if (ok && !name.isEmpty())
        m_itemModel->addGroup(name);
}

void MainWindow::onAddServer()
{
    auto sg = m_currentItem->findParent<ServerGroup>();
    if (sg)
    {
        auto db = QSqlDatabase::addDatabase(_S("QPSQL"), QUuid::createUuid().toString());
        auto server = Server(this);
        server.setCurrentGroup(sg->title());

        while(!db.isOpen() && server.exec() == QDialog::Accepted)
        {
            auto group = server.group();

            if (sg->title() != group)
            {
                auto serverGroups = m_currentItem->findParent<ServerGroups>();
                for(int i = 0; i < serverGroups->childCount(); i++)
                {
                    if (serverGroups->child(i)->title() == group)
                    {
                        sg = dynamic_cast<ServerGroup*>(serverGroups->child(i));
                        goto FOUND_GROUP;
                    }
                }

                sg = serverGroups->addGroup(group);
            }

FOUND_GROUP:
            db.setConnectOptions(_S("dbname=%1").arg(server.maintenanceDb()));
            db.setHostName(server.host());
            db.setPort(server.port());
            //server.service()
            db.setUserName(server.username());
            db.setPassword(server.password());
            //server.storePassword() const;
            //server.colour();

            if (db.open())
                sg->addServer(server.name(), db);
            else
            {
                QMessageBox::critical(this, tr("Error"), db.driver()->lastError().databaseText());
            }
        }
    }
}

void MainWindow::onDeleteServer()
{
    auto svr = m_currentItem->findParent<ServerItem>();
    if (svr)
    {
        auto ret = QMessageBox::question(this, tr("Delete Server"),
                                        tr("Are you sure you want to delete '<b>%1</b>'?").arg(svr->title()));
        if (ret == QMessageBox::Yes)
        {
            auto sg = m_currentItem->findParent<ServerGroup>();
            assert(sg != nullptr);

            sg->deleteServer(svr);
        }
    }
}

void MainWindow::onDisconnectServer()
{

}

void MainWindow::onDisconnectDatabase()
{
    auto tvObjectBrowser = m_ui->tvObjectBrowser;
    tvObjectBrowser->collapse(m_itemModel->indexOf(m_currentDatabase));
    m_currentDatabase->disconnectDatabase();
}

void MainWindow::onViewData()
{
    auto relation = m_currentItem->findParent<RelationItem>();
    if(relation)
    {
        auto db = m_currentDatabase->handle();
        auto schema = relation->findParent<SchemaItem>();

        auto query = makeQuery();

        QStringList fields;
        for(const auto& column : relation->fieldList())
        {
            fields << escapeIdentifier(db, column.name);
        }

        auto sql = _S("select %3\nfrom %1.%2\nlimit 1000\n").arg(
            escapeIdentifier(db, schema->title()), escapeIdentifier(db, relation->title()),
            fields.join(_S(", ")));

        QMetaObject::invokeMethod(query, "executeQuery", Qt::QueuedConnection,
                                  Q_ARG(QString, sql));
    }
}

#ifndef NDEBUG
#include <QTestEventList>

void MainWindow::test_selectTreeItem()
{
    auto tvObjectBrowser = m_ui->tvObjectBrowser;

    for(int i = 0; i < 10; i++)
    {
        tvObjectBrowser->expandAll();

        QApplication::processEvents();

        tvObjectBrowser->collapseAll();

        QApplication::processEvents();
    }

    tvObjectBrowser->expandAll();

    QTestEventList events;

    auto findPoint = [&](TreeItem::ItemType _type, const QString &_title)
    {
        auto idx = m_itemModel->findItem(_type, _title);
        assert(idx.isValid());
        return tvObjectBrowser->visualRect(idx).center();
    };

    auto click = [&](TreeItem::ItemType _type, const QString &_title)
    {
        events.addMouseClick(Qt::LeftButton, Qt::NoModifier, findPoint(_type, _title));
    };

    auto openTree = [&](TreeItem::ItemType _type, const QString &_title)
    {
        click(_type, _title);
        events.addKeyClick('+');
    };


    openTree(TreeItem::itServer,            _S("Localhost"));
    openTree(TreeItem::itGenericSection,    _S("Databases"));
    openTree(TreeItem::itDatabase,          _S("postgres"));
    openTree(TreeItem::itGenericSection,    _S("Catalogs"));
    openTree(TreeItem::itCatalog,           _S("Postgres"));
    openTree(TreeItem::itGenericSection,    _S("Tables"));

    DEBUG_MSG("start tests\n");
    events.simulate(tvObjectBrowser);
    DEBUG_MSG("end tests" << endl);

    close();
}
#endif
