#pragma once

#include <QObject>
#include <QAbstractItemView>
#include <QHelpEvent>
#include <QToolTip>

class QToolTipper : public QObject
{
    Q_OBJECT
public:
    explicit QToolTipper(QObject *parent = nullptr);

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};

// kate: indent-mode cstyle; indent-width 4; replace-tabs on;
