#include "main.h"
#include "sqlsyntaxhighlighter.h"

#include <QTextDocument>
#include <unordered_map>

#include <QSqlDriver>
#include <QSqlQuery>
#include <libpq-fe.h>

auto pgconn(QSqlDatabase &conn)
{
    auto handle = conn.driver()->handle();
    return *static_cast<PGconn **>(handle.data());
}

QStringList& getKeywords(QSqlDatabase &db)
{
    static std::unordered_map<QString, int> s_connectionServerVersions;
    static std::unordered_map<int, QStringList> s_serverVersionsKeywords;

    auto connName = db.connectionName();

    auto itServerVer = s_connectionServerVersions.find(connName);
    if(itServerVer == s_connectionServerVersions.end())
    {
        auto ver = PQserverVersion(pgconn(db));
        auto [it, inserted] = s_connectionServerVersions.emplace(connName, ver);
        assert(inserted);
        if(inserted)
            itServerVer = it;
    }

    auto ver = itServerVer->second;
    auto itVersionsKeywords = s_serverVersionsKeywords.find(ver);
    if(itVersionsKeywords == s_serverVersionsKeywords.end())
    {
        auto sql = QSqlQuery(db);
        sql.exec(_S("select * from pg_get_keywords()"));

        QStringList keywords;
        auto keyMask = _S("\\b%1\\b");
        while(sql.next())
        {
            keywords.push_back(keyMask.arg(sql.value(_S("word")).toString()));
        }

        auto [it, inserted] = s_serverVersionsKeywords.emplace(ver, std::move(keywords));
        assert(inserted);
        if(inserted)
            itVersionsKeywords = it;
    }

    return itVersionsKeywords->second;
}

SQLSyntaxHighlighter::SQLSyntaxHighlighter(QSqlDatabase &_db, QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
#ifdef __linux__
    document()->setDefaultFont(_S("Monospace"));
#else
    document()->setDefaultFont(_S("Consolas"));
#endif

    HighlightingRule rule;

#define CO(S) QColor(_S( S ))

    std::vector<QColor> colours;

    if (QPGAdminConfig.darkTheme)
        colours = {Qt::darkRed, Qt::darkMagenta, Qt::darkGreen, Qt::darkYellow, Qt::darkGreen, Qt::darkBlue};
    else
        colours = {CO("#18ebef"), CO("#ec5a7c"), CO("#539c6b"), CO("#ffffaf"), CO("#39d358"), CO("#0094fe")};

    keywordFormat.setForeground(colours[5]);
    const QStringList &keywordPatterns = getKeywords(_db);

    foreach (const QString &pattern, keywordPatterns) {
        rule.pattern = QRegularExpression(pattern, QRegularExpression::CaseInsensitiveOption);
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }

    //classFormat.setFontWeight(QFont::Bold);
//     classFormat.setForeground(Qt::darkMagenta);
//     rule.pattern = QRegularExpression(_S("\\bQ[A-Za-z]+\\b"));
//     rule.format = classFormat;
//     highlightingRules.append(rule);

    singleLineCommentFormat.setForeground(colours[0]);
    rule.pattern = QRegularExpression(_S("\'.*\'"));
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);

    quotationFormat.setForeground(colours[1]);
    rule.pattern = QRegularExpression(_S("\".*\""));
    rule.format = quotationFormat;
    highlightingRules.append(rule);

    //functionFormat.setFontItalic(true);
//     functionFormat.setForeground(Qt::darkGray);
//     rule.pattern = QRegularExpression(_S("\\b[A-Za-z0-9_]+(?=\\()"));
//     rule.format = functionFormat;
//     highlightingRules.append(rule);

    //commentStartExpression = QRegularExpression(_S("--"));
    //commentEndExpression = QRegularExpression(_S("$"));
    commentFormat.setFontItalic(true);
    commentFormat.setForeground(colours[2]);
    rule.pattern = QRegularExpression(_S("(\\s|^)(--.*$)"));
    rule.format = commentFormat;
    highlightingRules.append(rule);

    castFormat.setForeground(colours[3]);
    rule.pattern = QRegularExpression(_S("::[A-Za-z0-9_]+"));
    rule.format = castFormat;
    highlightingRules.append(rule);

    numberFormat.setForeground(colours[4]);
    rule.pattern = QRegularExpression(_S("\\b[0-9_]+\\b"));
    rule.format = numberFormat;
    highlightingRules.append(rule);
}

void SQLSyntaxHighlighter::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, highlightingRules) {
        QRegularExpressionMatchIterator matchIterator = rule.pattern.globalMatch(text);
        while (matchIterator.hasNext()) {
            QRegularExpressionMatch match = matchIterator.next();
            setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
    }
    setCurrentBlockState(0);

//     int startIndex = 0;
//     if (previousBlockState() != 1)
//         startIndex = text.indexOf(commentStartExpression);
//
//     while (startIndex >= 0) {
//         QRegularExpressionMatch match = commentEndExpression.match(text, startIndex);
//         int endIndex = match.capturedStart();
//         int commentLength = 0;
//         if (endIndex == -1) {
//             setCurrentBlockState(1);
//             commentLength = text.length() - startIndex;
//         } else {
//             commentLength = endIndex - startIndex
//                             + match.capturedLength();
//         }
//         setFormat(startIndex, commentLength, multiLineCommentFormat);
//         startIndex = text.indexOf(commentStartExpression, startIndex + commentLength);
//     }
}
