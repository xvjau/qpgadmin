import QtQuick 2.6
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: root
    width: 300
    height: 480
    visible: true
    property alias type: cmbType

    Label {
        id: server
    }

    TreeView {
        TableViewColumn {
            title: "Name"
            role: "fileName"
            width: 300
        }
        TableViewColumn {
            title: "Permissions"
            role: "filePermissions"
            width: 100
        }
        model: fileSystemModel
    }
}
