#include "main.h"
#include "ui/mainwindow.h"

#include <QCommandLineParser>
#include <QGuiApplication>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QTextCodec>
#include <QFile>

#include <csignal>
#include <cstdio>
#include <cassert>
#include <iostream>

#include <QWidget>
#include <QApplication>
#include <QTreeView>
#include <QGridLayout>

#ifndef NDEBUG
#include <QTimer>
#endif

#ifdef WIN32
#include <windows.h>
#else
#include <termios.h>
#include <unistd.h>
#endif

typeof(QPGAdminConfig) QPGAdminConfig;

std::tuple<bool,QString> readResource(const QString &_resource)
{
    QFile file(_resource);
    auto isOpen = file.open(QIODevice::ReadOnly);
    auto result = QString::fromUtf8(file.readAll());
    file.close();

    if(isOpen && !result.isEmpty())
        return {true, result};
    else
    {
        DEBUG_MSG("\nWARNING - Resource not found: " << _resource << endl);
        return {false, QString()};
    }
}

void setStdinEcho(bool enable = true)
{
#ifdef WIN32
    HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
    DWORD mode;
    GetConsoleMode(hStdin, &mode);

    if( !enable )
        mode &= ~ENABLE_ECHO_INPUT;
    else
        mode |= ENABLE_ECHO_INPUT;

    SetConsoleMode(hStdin, mode );

#else
    struct termios tty;
    tcgetattr(STDIN_FILENO, &tty);
    if( !enable )
        tty.c_lflag &= ~ECHO;
    else
        tty.c_lflag |= ECHO;

    (void) tcsetattr(STDIN_FILENO, TCSANOW, &tty);
#endif
}

int main(int argc, char **argv)
{
#ifndef NDEBUG
    bool runUnitTests = false;
#endif

    auto codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);
    QApplication app(argc, argv);
    app.setApplicationName(_S("qpgadmin"));
    app.setApplicationDisplayName(_S("qpgAdmin"));
    app.setOrganizationName(_S("Postgres"));
    app.setOrganizationDomain(_S("postgres.org"));

    MainWindow *mw = nullptr;

    if (argc > 1)
    {
        auto db = QSqlDatabase::addDatabase(_S("QPSQL"), _S("dbmain_postgres"));

        QCommandLineParser parser;
        parser.addHelpOption();
        parser.addVersionOption();

#ifndef NDEBUG
        QCommandLineOption unitTests(QStringList() << _C('t') << _S("unit-test"), QObject::tr("run unit tests"));
        parser.addOption(unitTests);
#endif

        QCommandLineOption host(QStringList() << _C('h') << _S("host"), QObject::tr("database server host or socket directory (default: \"%1\")").arg(_S("local socket")));
        QCommandLineOption port(QStringList() << _C('p') << _S("port"), QObject::tr("database server port (default: \"%1\")").arg(5432));
        QCommandLineOption username(QStringList() << _C('U') << _S("username"), QObject::tr("database user name (default: \"%1\"))")
#ifdef __unix__
            .arg(QString::fromLocal8Bit(qgetenv("USER")))
#else
            .arg(QString::fromLocal8Bit(qgetenv("USERNAME")))
#endif
        );
        QCommandLineOption no_password(QStringList() << _C('W') << _S("no-password"), QObject::tr("never prompt for password"));
        QCommandLineOption password(QStringList() << _C('w') << _S("password"),
                                    QObject::tr("force password prompt (should happen automatically)"));

        QCommandLineOption darkTheme(QStringList() << _C('d') << _S("dark"),
                                    QObject::tr("use dark theme"));

        parser.addOption(host);
        parser.addOption(port);
        parser.addOption(username);
        parser.addOption(no_password);
        parser.addOption(password);
        parser.addOption(darkTheme);
        // Process the actual command line arguments given by the user
        parser.process(app);
        if (parser.isSet(host)) db.setHostName(parser.value(host));
        if (parser.isSet(port)) db.setPort(parser.value(port).toUShort());
        if (parser.isSet(username)) db.setUserName(parser.value(username));

        if (parser.isSet(host) || parser.isSet(port) || parser.isSet(username))
        {
            if(!parser.isSet(no_password))
            {
                if (parser.isSet(password))
                    db.setPassword(parser.value(password));
                else
                {
                    if (argc > 1)
                    {
                        std::string p;
                        std::cout << tr("password:");
                        setStdinEcho(false);
                        std::cin >> p;
                        setStdinEcho(true);
                        db.setPassword(QString::fromStdString(p));
                    }
                }
            }

            db.setConnectOptions(_S("dbname=postgres"));

            if (!db.open())
            {
                auto driver = db.driver();
                auto error = driver->lastError();

                std::cerr << error.text() << "\n" << error.driverText() << std::endl;
            }

            mw = new MainWindow(db);
        }

#ifndef NDEBUG
        runUnitTests = parser.isSet(unitTests);
#endif
    }

    if (!mw)
    {
        mw = new MainWindow();
    }

    mw->show();
#ifndef NDEBUG
    if (runUnitTests)
    {
        auto timer = new QTimer();
        timer->setSingleShot(true);
        timer->setInterval(std::chrono::milliseconds(500));
        QObject::connect(timer, &QTimer::timeout, [&mw, timer]
        {
            mw->test_selectTreeItem();
            timer->deleteLater();
        });
        timer->start();
    }
#endif

//#include <QQmlApplicationEngine>
//#include <QQuickView>
//#include <QQmlContext>

//     QQmlApplicationEngine engine;
//     QObject::connect(&engine, &QQmlApplicationEngine::warnings,
//                     [](const QList<QQmlError> &warnings)
//                     {
//                         for(auto& w : warnings)
//                         {
//                             std::cerr << w.toString() << std::endl;
//                         }
//                     }
//     );
//
//     engine.addImportPath(_S("qrc:/"));
//     engine.addImportPath(_S("qrc:/ui/"));
//
//     auto rootCtx = engine.rootContext();
//     rootCtx->setContextProperty(_S("databases"), &databases);
//
//     QQuickView view;
//     view.setSource(QUrl(_S("qrc:/ui/main.qml")));

    return app.exec();
}
