/*
 * <B1tF0rge> 2018
 */

#pragma once

#include <iostream>
#include <QString>
#include <QVariant>
#include <QtSql/QSqlDatabase>

#ifndef NDEBUG
#define DEBUG_MSG(STR) do { using namespace std; cerr << STR << flush; } while (false)
#else
#define DEBUG_MSG(STR) do {  } while (false)
#endif

#ifdef __GNUC__
#define _S(STR) QStringLiteral(STR)
#define _C(CHR) QChar::fromLatin1(CHR)
#else
#define _S(STR) QString::fromUtf8(STR)
#define _C(CHR) QChar::fromLatin1(CHR)
#endif

typedef uint32_t oid_t;

inline std::istream &operator>>(std::istream &stream, QString &string)
{
    std::string s;
    stream >> s;
    string = QString::fromStdString(s);
    return stream;
}

inline std::ostream &operator<<(std::ostream &stream, const QString &string)
{
    stream << string.toLocal8Bit().data();
    return stream;
}

inline std::ostream &operator<<(std::ostream &stream, const QByteArray &string)
{
    stream << string.data();
    return stream;
}

inline std::ostream &operator<<(std::ostream &stream, const QVariant &string)
{
    stream << string.toString();
    return stream;
}

inline QString &operator<<(QString &_str, char _c)
{
    _str += QChar::fromLatin1(_c);
    return _str;
}

inline QString &operator<<(QString &_str, int _i)
{
    _str += QString::number(_i);
    return _str;
}

template<typename T>
inline QString &operator<<(QString &_str, const T &_t)
{
    _str += _t;
    return _str;
}

inline std::ostream &operator<<(std::ostream &stream, const QSqlDatabase &db)
{
    stream << db.hostName() << ":" << db.port() << " " << db.databaseName() << " " << db.userName();
    return stream;
}

inline auto tr(const char *str)
{
    return QObject::tr(str);
}

namespace std {

#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
template<>
struct hash<QString> : public hash<std::string>
{
    size_t
    operator()(const QString &_str) const noexcept
    {
        return hash<std::string>()(_str.toStdString());
    }
};
#endif // QT_VERSION

} // namespace std

#include <QtGui/QCursor>
#include <QtWidgets/QApplication>

class CursorGuard final
{
public:
    CursorGuard(const QCursor &_cursor = Qt::WaitCursor)
    {
        QApplication::setOverrideCursor(_cursor);
        QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
    }

    ~CursorGuard()
    {
        reset();
    }

    void reset()
    {
        QApplication::restoreOverrideCursor();
    }
};

std::tuple<bool, QString> readResource(const QString &_resource);

inline QStringList pgArrayToList(const QString &_str)
{
    auto result = _str.split(_C(','));
    result[0] = result[0].mid(1);
    result[result.count() - 1].chop(1);

    if(result.count() == 1 && result[0].isEmpty())
        return QStringList();

    return result;
}

inline QStringList pgArrayToList(const QVariant &_val)
{
    return pgArrayToList(_val.toString());
}

#include <libpq-fe.h>

inline QString escpaeLiteral(QVariant _handle, const QString &_str)
{
    auto conn = *static_cast<PGconn **>(_handle.data());
    auto ba = _str.toUtf8();
    auto str = PQescapeLiteral(conn, ba.data(), ba.size());
    auto result = QString::fromUtf8(str);
    PQfreemem(str);
    return result;
}

inline QString escapeIdentifier(QVariant _handle, const QString &_str)
{
    auto conn = *static_cast<PGconn **>(_handle.data());
    auto ba = _str.toUtf8();
    auto str = PQescapeIdentifier(conn, ba.data(), ba.size());
    auto result = QString::fromUtf8(str);
    PQfreemem(str);
    return result;
}

extern struct QPGAdminConfig_t
{
    bool darkTheme = false;
} QPGAdminConfig;

